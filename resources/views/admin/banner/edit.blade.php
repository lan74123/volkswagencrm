@extends('admin.master')
@inject('BackendPresenter', 'App\Presenter\BackendPresenter')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<form id="EditForm" class="form-horizontal" method="post" enctype="multipart/form-data" action="{{ url('/backend/banner/modify') }}">
				{{ csrf_field() }}
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h4 class="panel-title">Banner Edit</h4>
					</div>
					<div class="panel-body">
						<div>
							<input name="id" type="readonly" value="{{ $banner->id }}" id="id"  style="display:none">
							<!-- 表格本體 -->
							<table class="table" cellspacing="0" id="DetailsView1" style="border-collapse:collapse;">
								<tbody>	
									<!-- 欄位：Title -->
									<tr>
										<td class="header-require col-lg-2"><span style="color:red">*</span>Title</td>
										<td>
											<div class="col-lg-4 nopadding">											
												<input name="title" type="text" maxlength="100" id="title" class="form-control" value="{{ $banner->title }}">											
												<label class="error" for="title"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：Banner -->								
									<tr>
										<td class="header-require col-lg-2"><span style="color:red">*</span>Banner</td>
										<td>
											<div class="col-lg-4 nopadding">
												<input name="banner" type="file" id="banner" class="form-control" value="">
												<img src="/storage/{{ $banner->banner }}" height="100">
												<label class="error" for="banner"></label>
											</div>
										</td>
									</tr>						
									<!-- 下控制按鈕 -->
									<tr>
										<td>&nbsp;</td>
										<td>
											<div style="text-align: right">			
												<a class="btn btn-xs btn-default" href="{{ url('/backend/banner') }}">Back</a>								
												<input type="submit" name="btnUpdate_foot" value="create" id="btnUpdate_foot" class="btn btn-primary btn-xs" onclick="submitForm();">
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- panel-body -->
				</div>
			</form>
		</div>
	</div>
@endsection

@section('extjs')
	<script>
	$(document).ready(function() 
	{		
		//初始化需要偵錯的表格
		$('#EditForm').validate();
		//正規表達驗證初始化
		$.validator.addMethod(
			"regex",
			function (value, element, regexp) 
			{
				var re = new RegExp(regexp);
				return this.optional(element) || re.test(value);
			}
		);
		
		//各欄位
		$('#name').rules("add", 
		{
			required: true,
			minlength: 1,
			maxlength: 20,
			messages: {
				required: "name length must between 1-20",
				minlength: "name length must between 1-20",
				maxlength: "name length must between 1-20"
			}
		});		
		
	});
	//提交與取消按鈕
	function submitForm() 
	{
		if (!!($("#EditForm").valid()) === false) 
		{
			return false;
		} 
		else 
		{
			$(document).ready(function() 
			{
				$.blockUI({ css: {
					border: 'none',
					padding: '15px',
					backgroundColor: '#000',
					'-webkit-border-radius': '10px',
					'-moz-border-radius': '10px',
					opacity: .5,
					color: '#fff'
				}});
			});
		}
	}

	function cancelValidate() 
	{
		$("#EditForm").validate().cancelSubmit = true;
	}
	</script>
@endsection
