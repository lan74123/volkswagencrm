@extends('admin.master')
@inject('BackendPresenter', 'App\Presenter\BackendPresenter')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-dark">
				<div class="panel-heading">
					<div class="row">
						<div class="form-group">
							<div class="panel-title">Manage Version</div>
						</div>
						<div class="form-inline">
                            <form method="post" action="{{ asset('/backend/version') }}" name="searchFrom">
								{{ csrf_field() }}      
								<div class="form-group" >
									<label class="control-label" style="color:white;">裝置 <br/>Device:</label>									
									<select id="device" name="device" class="form-control">
										<option value="">不拘</option>
										<option value="iOS">iOS</option>
										<option value="Android">Android</option>
									</select>
								</div>                         
                                <div class="form-group" >
                                    <label class="control-label" style="color:white;">版本 <br/>Version:</label><input type="text" name="version" value="{{ $cond->version }}"  class="form-control">
								</div>														
                                <div class="form-group" >
                                    <label class="control-label" style="color:white;">是否強制更新 <br/>Force Update:</label>
                                    <select name="is_update" class="form-control">
										<option value="" {{ ($cond->is_update == "") ? 'selected' : '' }}>不拘</option>
                                        <option value="1" {{ ($cond->is_update == "1") ? 'selected' : '' }}>是 Yes</option>
                                        <option value="0" {{ ($cond->is_update == "0") ? 'selected' : '' }}>否 No</option>
                                    </select>
								</div>
								<div class="form-group" >
										<label class="control-label" style="color:white;">URL </label><input type="text" name="url" value="{{ $cond->url }}"  class="form-control">
									</div>
                                <div class="form-group" >
                                    <a id="btnOK" name="btnOK"  class="btn btn-danger btn-xs" >Search</a>
									<a class="btn btn-darkblue btn-xs" href="{{ asset('backend/version/create') }}"><strong>Add</strong></a>									
                                </div>
                            </form>
                        </div>						
					</div>
				</div>
				<table class="table table-hover" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>編號</th>
							<th>裝置</th>
							<th>版本</th>
							<th>是否強制更新</th>
							<th>訊息</th>
							<th>URL</th>						
							<th>創建時間</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach($tables as $data)
							<tr>
								<td>{{ $data->id }}</td>
								<td>{{ $data->device }}</td>
								<td>{{ $data->version }}
								<td>{{ ($data->is_update)? '是':'否' }}</td>
								<td>{{ $data->message }}</td>								
								<td>{{ $data->url }}</td>
								<td>{{ $data->created_at }}</td>
								<td style="text-align: right">
									<form method="post" action="{{ asset('/backend/version/delete/'.$data->id) }}">
										<a class="btn btn-warning btn-xs" href="{{ asset('/backend/version/edit/'.$data->id) }}">
											<strong>Edit</strong>
										</a>
										{{ csrf_field() }}
										{{ method_field('DELETE') }}
										<button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('即將永久刪除此筆資料，確認繼續刪除?')"><strong>Delete</strong></button>
									</form>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<script>
        $(function () 
		{
            $("#date_start").datepicker({ "dateFormat": "yy-mm-dd" });
            $("#date_end").datepicker({ "dateFormat": "yy-mm-dd" });

            //判斷活動開始結束日期
            $("#btnOK").on("click", function () 
			{               
                    $('form[name="searchFrom"]').submit();                
            });

        });

        //判斷開始日期不得大於結束日期
        function  checkDate() 
		{
            var beginDate=$("#date_start").val();
            var endDate=$("#date_end").val();
            var d1 = new Date(beginDate.replace(/-/g, "/"));
            var d2 = new Date(endDate.replace(/-/g, "/"));

            if(beginDate!=""&&endDate!=""&&d1 >d2)
            {
                alert("開始日期不能晚於結束日期！\n The start date can not be later than the end date !");
                return false;
            }
			else
			{
                return true;
            }
        };

		//下載XLSX
		function downloadExcel(action) 
		{		 		
			$('form[name="searchFrom"]').attr("action", action);
			$('form[name="searchFrom"]').submit();
        };


    </script>
@endsection
