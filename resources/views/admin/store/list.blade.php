@extends('admin.master')
@inject('BackendPresenter', 'App\Presenter\BackendPresenter')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-dark">
				<div class="panel-heading">
					<div class="row">
						<div class="form-group">
							<div class="panel-title">Manage store</div>
						</div>
						<div class="form-inline">
                            <form method="post" action="{{ asset('/backend/store') }}" name="searchFrom">
								{{ csrf_field() }}      
								<div class="form-group" >
									<label class="control-label" style="color:white;">經銷商 <br/>Dealer:</label>									
									{!! $BackendPresenter->getDealerSelectlist($cond->dealer , $cond->disabled , $cond->allplace) !!}
								</div>								
								<div class="form-group" >
									<label class="control-label" style="color:white;">服務中心區域 <br/>Area:</label>									
									{!! $BackendPresenter->getAreaSelectlist($cond->area , '' , true) !!}
								</div>  
								<div class="form-group" >
									<label class="control-label" style="color:white;">服務中心類型 <br/>Type:</label>
									{!! $BackendPresenter->getServiceSelectlist($cond->type , '' , true) !!}										
									</div>                       
                                <div class="form-group" >
                                    <label class="control-label" style="color:white;">服務中心名稱 <br/>StoreName:</label><input type="text" name="name" value="{{ $cond->name }}"  class="form-control">
								</div>
								<div class="form-group" >
									<label class="control-label" style="color:white;">服務中心電話 <br/>StorePhone:</label><input type="text" name="tel" value="{{ $cond->tel }}"  class="form-control">
								</div>																
                                <div class="form-group" >
                                    <label class="control-label" style="color:white;">是否有效 <br/>Status:</label>
                                    <select name="valid" class="form-control">
										<option value="" {{ ($cond->valid == "") ? 'selected' : '' }}>不拘</option>
                                        <option value="1" {{ ($cond->valid == "1") ? 'selected' : '' }}>有效 Valid</option>
                                        <option value="0" {{ ($cond->valid == "0") ? 'selected' : '' }}>無效 Invalid</option>
                                    </select>
								</div>
                                <div class="form-group" >
                                    <a id="btnOK" name="btnOK"  class="btn btn-danger btn-xs" >Search</a>
									<a class="btn btn-darkblue btn-xs" href="{{ asset('backend/store/'.$dealerId.'/create') }}"><strong>Add</strong></a>	
									@if($cond->allplace)
									<a class="btn btn-default btn-xs" href="{{ asset('/backend/dealer') }}">back</a>		
									@endif						
                                </div>
                            </form>
                        </div>						
					</div>
				</div>
				<table class="table table-hover" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>編號</th>
							<th>經銷商</th>
							<th>區域</th>
							<th>服務中心類型</th>
							<th>中心名稱</th>
							<th>營業時間</th>
							<th>連絡電話</th>
							<th>連絡地址</th>
							<th>營業時間</th>							
							<th>建立日期</th>
							<th>狀態</th>							
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach($tables as $data)
							<tr>
								<td>{{ $data->id }}</td>
								<td>{{ $data->dealer->name }}</td>
								<td>{{ $data->area }}</td>
								<td>{!! $BackendPresenter->getServiceName($data->type, false) !!}</td>
								<td>{{ $data->name }}</td>
								<td>{{ $data->servicetime }}</td>
								<td>{{ $data->tel }}</td>								
								<td>{{ $data->address }}</td>
								<td>{{ $data->servicetime }}</td>
								<td>{{ $data->created_at }}</td>
								<td>{{ ( $data->valid )? '有效':'無效' }}</td>								
								<td style="text-align: right">
									<form method="post" action="{{ asset('/backend/store/delete/' . $data->id) }}">
										<a class="btn btn-warning btn-xs" href="{{ asset('/backend/store/' . $data->dealer->id . '/edit/' . $data->id) }}">
											<strong>Edit</strong>
										</a>
										{{ csrf_field() }}
										{{ method_field('DELETE') }}
										<button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('即將永久刪除此筆服務中心資料，確認繼續刪除?')"><strong>Delete</strong></button>
									</form>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				<div class="col-md-12 text-center no-margin">
						<?php echo $tables->render(); ?>
				</div>
			</div>
		</div>
	</div>

	<script>
        $(function () 
		{
            $("#date_start").datepicker({ "dateFormat": "yy-mm-dd" });
            $("#date_end").datepicker({ "dateFormat": "yy-mm-dd" });

            //判斷活動開始結束日期
            $("#btnOK").on("click", function () 
			{               
                    $('form[name="searchFrom"]').submit();                
            });

        });

        //判斷開始日期不得大於結束日期
        function  checkDate() 
		{
            var beginDate=$("#date_start").val();
            var endDate=$("#date_end").val();
            var d1 = new Date(beginDate.replace(/-/g, "/"));
            var d2 = new Date(endDate.replace(/-/g, "/"));

            if(beginDate!=""&&endDate!=""&&d1 >d2)
            {
                alert("開始日期不能晚於結束日期！\n The start date can not be later than the end date !");
                return false;
            }
			else
			{
                return true;
            }
        };

		//下載XLSX
		function downloadExcel(action) 
		{		 		
			$('form[name="searchFrom"]').attr("action", action);
			$('form[name="searchFrom"]').submit();
        };


    </script>
@endsection
