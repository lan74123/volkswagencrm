@extends('admin.master')
@inject('BackendPresenter', 'App\Presenter\BackendPresenter')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<form id="EditForm" class="form-horizontal" method="post" action="{{ asset('/backend/dealer/modify') }}">
				{{ csrf_field() }}
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h4 class="panel-title">Dealer Add</h4>
					</div>
					<div class="panel-body">
						<div>
								<input name="id" type="readonly" value="{{ $dealer->id }}" id="id"  style="display:none">
							<!-- 表格本體 -->
							<table class="table" cellspacing="0" id="DetailsView1" style="border-collapse:collapse;">
								<tbody>	
									<!-- 欄位：area -->
									<tr>
										<td class="header-require col-lg-2">經銷商區域</td>
										<td >
											<div class="col-lg-4 nopadding">	
											{!! $BackendPresenter->getAreaSelectlist($dealer->area,'',false) !!}
											</div>
										</td>
									</tr>								
									<!-- 欄位：name -->
									<tr>
										<td class="header-require col-lg-2"><span style="color:red">*</span>經銷商名稱</td>
										<td>
											<div class="col-lg-4 nopadding">											
												<input name="name" type="text"  maxlength="30" id="name" class="form-control" value="{{$dealer->name}}">											
												<label class="error" for="name"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：code -->
									<tr>
										<td class="header-require col-lg-2">經銷商代號</td>
										<td>
											<div class="col-lg-4 nopadding">											
												<input name="code" type="text"  maxlength="10" id="code" class="form-control" value="{{$dealer->code}}">											
												<label class="error" for="code"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：tel -->
									<tr>
										<td class="header-require col-lg-2">經銷商連絡電話</td>
										<td>
											<div class="col-lg-4 nopadding">
												<input name="tel" type="text"  maxlength="20" id="tel" class="form-control" value="{{$dealer->tel}}">
												<label class="error" for="tel"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：fax -->
									<tr>
										<td class="header-require col-lg-2">經銷商傳真號碼</td>
										<td>
											<div class="col-lg-4 nopadding">
												<input name="fax" type="text"  maxlength="20" id="fax" class="form-control" value="{{$dealer->fax}}">
												<label class="error" for="fax"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：address -->
									<tr>
										<td class="header-require col-lg-2">經銷商地址</td>
										<td>
											<div class="col-lg-8 nopadding">
												<input name="address" type="text"  maxlength="100" id="address" class="form-control" value="{{$dealer->address}}">
												<label class="error" for="address"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：email -->								
									<tr>
										<td class="header-require col-lg-2">經銷商信箱</td>
										<td>
											<div class="col-lg-4 nopadding">
												<input name="email" type="text"  maxlength="100" id="email" class="form-control" value="{{$dealer->email}}">
												<label class="error" for="email"></label>
											</div>
										</td>
									</tr>	
									<!-- 欄位：emailcc -->								
									<tr>
										<td class="header-require col-lg-2">經銷商信箱副本</td>
										<td>
											<div class="col-lg-4 nopadding">
												<input name="emailcc" type="text" maxlength="100" id="emailcc" class="form-control" value="{{$dealer->emailcc}}">
												<label class="error" for="emailcc"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：servicetime -->								
									<tr>
										<td class="header-require col-lg-2">經銷商營業時間</td>
										<td>
											<div class="col-lg-4 nopadding">
												<textarea name="servicetime" type="text"  cols="5" rows="5" maxlength="100" id="servicetime" class="form-control">{{$dealer->servicetime}}</textarea>
												<label class="error" for="servicetime"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：valid -->
									<tr>
										<td class="header-require col-lg-2">是否有效</td>
										<td>
											<div class="col-lg-5 nopadding">
												<input type="checkbox" name="valid" {{ ($dealer->valid == '1') ? 'checked' : '' }}>
												<label class="error" for="valid"></label>
											</div>
										</td>
									</tr>									
									<!-- 下控制按鈕 -->
									<tr>
										<td>&nbsp;</td>
										<td>
											<div style="text-align: right">			
												<a class="btn btn-xs btn-default" href="{{ asset('/backend/dealer') }}">back</a>								
												<input type="submit" name="btnUpdate_foot" value="update" id="btnUpdate_foot" class="btn btn-primary btn-xs" onclick="submitForm();">
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- panel-body -->
				</div>
			</form>
		</div>
	</div>
@endsection

@section('extjs')
	<script>
	$(document).ready(function() 
	{

		//初始化需要偵錯的表格
		$('#EditForm').validate();
		//正規表達驗證初始化
		$.validator.addMethod(
			"regex",
			function (value, element, regexp) 
			{
				var re = new RegExp(regexp);
				return this.optional(element) || re.test(value);
			}
		);
		//各欄位
		$('#name').rules("add", 
		{
			required: true,
			minlength: 1,
			maxlength: 20,
			messages: {
				required: "name length must between 1-20",
				minlength: "name length must between 1-20",
				maxlength: "name length must between 1-20"
			}
		});
		
	});
	//提交與取消按鈕
	function submitForm() 
	{
		if (!!($("#EditForm").valid()) === false) 
		{
			return false;
		} 
		else 
		{
			$(document).ready(function() 
			{
				$.blockUI({ css: 
				{
					border: 'none',
					padding: '15px',
					backgroundColor: '#000',
					'-webkit-border-radius': '10px',
					'-moz-border-radius': '10px',
					opacity: .5,
					color: '#fff'
				}});
			});
		}
	}

	function cancelValidate() 
	{
		$("#EditForm").validate().cancelSubmit = true;
	}
	</script>
@endsection
