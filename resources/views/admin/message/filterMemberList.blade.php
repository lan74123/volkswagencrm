@extends('admin.master')
@inject('BackendPresenter', 'App\Presenter\BackendPresenter')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<nav class="navbar navbar-default">
					<div class="container-fluid">
						<div class="navbar-header">
							<a class="navbar-brand" href="#">Create Flow</a>
						</div>
						<ul class="nav navbar-nav">
							<li class="active"><a href="#">Search Member List</a></li>
							<li><a href="#"> > </a></li>
							<li><a href="#">Add</a></li>
							<li><a href="#"> > </a></li>
							<li><a href="#">Group Info</a></li>
							<li><a href="#"> > </a></li>
							<li><a href="#">Create</a></li>
						</ul>
					</div>
				</nav>
			</div>
			<div class="panel panel-primary">				
				<div class="panel-heading">						
						<div class="row">
								<div class="form-group">
									<div class="panel-title">Filter Member</div>
								</div>
								<div class="form-inline">
									<form method="post" action="{{ asset('/backend/messageGroup/filterMemberList') }}" name="searchFrom">
										{{ csrf_field() }} 
										<div class="form-group" >
												<label class="control-label" style="color:white;">建立起始日期 <br/>Start Date:</label><input type="text" value="{{ $cond->date_start }}" id="date_start" name="date_start" class="form-control" autocomplete="off">
											</div>
										<div class="form-group" >
											<label class="control-label" style="color:white;">建立結束日期 <br/>End Date:</label><input type="text" value="{{ $cond->date_end }}" id="date_end" name="date_end" class="form-control" autocomplete="off">
										</div>
										<div class="form-group" >												
											<a id="btnOK" name="btnOK"  class="btn btn-danger btn-xs" >Search</a>	
											<a id="btnAdd" name="btnAdd"  class="btn btn-info btn-xs" >Add</a>	
											<a class="btn btn-xs btn-default" href="{{ asset('/backend/messageGroup') }}">cancle</a>									
										</div>
									</form>
								</div>						
							</div>
						</div>
						@if($members > 0)
						<table class="table table-hover filtermember" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>編號</th>
									<th>姓名&Email</th>
									<th>連絡電話</th>
									<th>連絡地址</th>
									<th>同意資料使用</th>							
									<th>註冊日期</th>																
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($tables as $data)
									<tr>
										<td>{{ $data->id }}</td>
										<td>{{ $data->name }}<br>{{ $data->email }}</td>
										<td>{{ $data->mobile }}</td>								
										<td>{{ $data->address }}</td>
										<td>{{ ( $data->agree_data_use )? 'Y':'N' }}</td>
										<td>{{ $data->created_at }}</td>															
										<td style="text-align: right">											
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
						<div class="col-md-12 text-center no-margin">
								<?php echo $tables->render(); ?>
						</div>
						@else		
							<div class="form-group">
								<label class="control-label col-sm-2" for="email">查無資料</label>								
							</div>
						@endif
					</div>				
			</div>		
	</div>
	<script>
        $(function () 
		{            
			$("#date_start").datepicker({ "dateFormat": "yy-mm-dd" });
            $("#date_end").datepicker({ "dateFormat": "yy-mm-dd" });

            $("#btnOK").on("click", function () 
			{               
				if(checkDate())
				{
					$('form[name="searchFrom"]').submit();    
				}           
            });

			$("#btnAdd").on("click", function () 
			{    
				var member = "{{ $members }}";
				
				if(member > 0)
				{
					var url = "{{ asset('/backend/messageGroup/create') }}";
					$('form[name="searchFrom"]').attr('action', url);          
					$('form[name="searchFrom"]').submit();  
				}          
				else
				{
					alert('您選擇的條件查無資料，請重新搜尋。');
				}   
            });
        });

		//判斷開始日期不得大於結束日期
        function  checkDate() 
		{
            var beginDate=$("#date_start").val();
            var endDate=$("#date_end").val();
            var d1 = new Date(beginDate.replace(/-/g, "/"));
            var d2 = new Date(endDate.replace(/-/g, "/"));

            if(beginDate!=""&&endDate!=""&&d1 >d2)
            {
                alert("開始日期不能晚於結束日期！\n The start date can not be later than the end date !");
                return false;
            }
			else
			{
                return true;
            }
        };		
    </script>	
@endsection

