@extends('admin.master')
@inject('BackendPresenter', 'App\Presenter\BackendPresenter')

@section('content')
	<div class="row">
		<div class="col-lg-12">			
			<div class="panel panel-primary">			
				<form id="EditForm" class="form-horizontal" method="post" action="{{ asset('/backend/message/insert') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h4 class="panel-title">Message Add</h4>
						</div>
						<div class="panel-body">
							<div>
								<!-- 表格本體 -->
								<table class="table" cellspacing="0" id="DetailsView1" style="border-collapse:collapse;">
									<tbody>
										<!-- 欄位：name -->
										<tr>
											<td class="header-require col-lg-2"><span style="color:red">*</span>設定日期及時間</td>
												<td>													
												<div class="col-lg-6 nopadding">
													<div class="form-group">
															<div class="form-inline">																			
																	<input type="radio" name="sendTimeType" ng-model="sendTimeType" value="RESERVE" class="form-control">
																	<input id="date_push" type="text" name="sendDate" placeholder="YYYY-MM-DD" ng-model="date" class="form-control">
																																	
																	<select class="form-control" name="sendHour">
																			<option value="0" selected="">0</option>
																			<option value="1">1</option>
																			<option value="2">2</option>
																			<option value="3">3</option>
																			<option value="4">4</option>
																			<option value="5">5</option>
																			<option value="6">6</option>
																			<option value="7">7</option>
																			<option value="8">8</option>
																			<option value="9">9</option>
																			<option value="10">10</option>
																			<option value="11">11</option>
																			<option value="12">12</option>
																			<option value="13">13</option>
																			<option value="14">14</option>
																			<option value="15">15</option>
																			<option value="16">16</option>
																			<option value="17">17</option>
																			<option value="18">18</option>
																			<option value="19">19</option>
																			<option value="20">20</option>
																			<option value="21">21</option>
																			<option value="22">22</option>
																			<option value="23">23</option>
																	</select>
																	<label class="control-label" >: </label>																		
																	<select class="form-control" name="minutes_1">
																			<option value="0" selected="">0</option>
																			<option value="1">1</option>
																			<option value="2">2</option>
																			<option value="3">3</option>
																			<option value="4">4</option>
																			<option value="5">5</option>
																	</select>
																																	
																	<select class="form-control" name="minutes_2">
																			<option value="0" selected="">0</option>
																			<option value="1">1</option>
																			<option value="2">2</option>
																			<option value="3">3</option>
																			<option value="4">4</option>
																			<option value="5">5</option>
																			<option value="6">6</option>
																			<option value="7">7</option>
																			<option value="8">8</option>
																			<option value="9">9</option>
																	</select>
															</div>																	
													
															<div class="form-inline">
																	<input type="radio" name="sendTimeType" ng-model="sendTimeType" value="NOW" class="form-control"> 
																	<label class="control-label" >立即傳送 </label>
															</div>
													</div>
												</div>
											</td>
										</tr>		
										<!-- 欄位：GROUP -->
										<tr>
												<td class="header-require col-lg-2">訊息群組</td>
												<td>
													<div class="col-lg-6 nopadding">	
														{!! $BackendPresenter->getMessageGrouplist($cond->dealer , $cond->disabled , $cond->allplace) !!}
												</div>
											</td>
										</tr>														
										<!-- 欄位：name -->
										<tr>
											<td class="header-require col-lg-2"><span style="color:red">*</span>標題</br>(限三十字)</td>
											<td>
												<div class="col-lg-6 nopadding">											
													<input name="title" type="text" value="{{old('title')}}" maxlength="50" id="title" class="form-control">											
													<label class="error" for="title"></label>
												</div>
											</td>
										</tr>									
										<!-- 欄位：desc -->
										<tr>
											<td class="header-require col-lg-2"><span style="color:red">*</span>內容</br>(限兩百字)</td>
											<td>
												<div class="col-lg-6 nopadding">											
												<textarea name="content" type="text" value="" maxlength="200" id="content"  rows="10" class="form-control">{{old('content')}}</textarea>										
													<label class="error" for="content"></label>
												</div>
											</td>
										</tr>	
										 <!-- url -->
										<tr>
											<td class="header-require col-lg-2">APP外連網址</td>
											<td>
												<div class="col-lg-6 nopadding">											
													<input name="url" type="text" value="{{old('url')}}" maxlength="200" rows="2" id="url" class="form-control">											
													<label class="error" for="url"></label>
												</div>
											</td>
										</tr>
										<!-- 欄位：NewsID -->
										<tr>
											<td class="header-require col-lg-2">APP內連指定消息/活動連結</td>
											<td>
												<div class="col-lg-6 nopadding">											
													<div class="col-lg-6 nopadding">	
														{!! $BackendPresenter->getNewslist('', '', 'create') !!}
													</div>
												</div>
											</td>
										</tr>
										 <!-- 欄位：MsgIMG -->
										 <tr>
											<td class="header-require col-lg-2">圖片上傳</td>
											<td>
												<div class="col-lg-6 nopadding">											
													<input type="file" name="img_path" class="multi with-preview maxsize-3072" accept="png|jpeg|jpg" />
													JPG格式 1400*1049，大小不得超過3MB / JPG 1400*1049 Size under 3MB
												</div>
											</td>
										</tr>											
										<!-- 下控制按鈕 -->
										<tr>
											<td>&nbsp;</td>
											<td>
												<div style="text-align: right">			
													<a class="btn btn-xs btn-default" href="{{ asset('/backend/message') }}">cancle</a>								
													<input type="submit" name="btnUpdate_foot" value="create" id="btnUpdate_foot" class="btn btn-primary btn-xs" onclick="submitForm();">
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<!-- panel-body -->					
					</div>
				</form>		
			</div>		
	</div>
@endsection

@section('extjs')
	<script>
	$(document).ready(function() 
	{		
		$("#date_push").datepicker({ "dateFormat": "yy-mm-dd" });

		//初始化需要偵錯的表格
		$('#EditForm').validate();
		//正規表達驗證初始化
		$.validator.addMethod(
			"regex",
			function (value, element, regexp) 
			{
				var re = new RegExp(regexp);
				return this.optional(element) || re.test(value);
			}
		);
		//各欄位
		$('#title').rules("add", 
		{
			required: true,
			minlength: 1,
			maxlength: 50,
			messages: {
				required: "title length must between 1-50",
				minlength: "title length must between 1-50",
				maxlength: "title length must between 1-50"
			}
		});		
	});
	//提交與取消按鈕
	function submitForm() 
	{
		if (!!($("#EditForm").valid()) === false) 
		{
			return false;
		} 
		else 
		{
			$(document).ready(function() 
			{
				$.blockUI({ css: {
					border: 'none',
					padding: '15px',
					backgroundColor: '#000',
					'-webkit-border-radius': '10px',
					'-moz-border-radius': '10px',
					opacity: .5,
					color: '#fff'
				}});
			});
		}
	}

	function cancelValidate() 
	{
		$("#EditForm").validate().cancelSubmit = true;
	}
	</script>
@endsection
