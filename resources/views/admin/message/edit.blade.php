@extends('admin.master')
@inject('BackendPresenter', 'App\Presenter\BackendPresenter')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<form id="EditForm" class="form-horizontal" method="post" action="{{ asset('/backend/message/modify') }}">
				{{ csrf_field() }}
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h4 class="panel-title">message Edit</h4>
					</div>
					<div class="panel-body">
						<div>
							<input name="id" type="readonly" value="{{ $message->id }}" id="id"  style="display:none">
							<!-- 表格本體 -->
							<table class="table" cellspacing="0" id="DetailsView1" style="border-collapse:collapse;">
								<tbody>									
									<!-- 欄位：name -->
									<tr>
											<td class="header-require col-lg-2"><span style="color:red">*</span>溝通群組名稱</td>
											<td>
												<div class="col-lg-4 nopadding">											
													<input name="name" type="text" value="{{ $message->title }}" maxlength="50" id="name" class="form-control">											
													<label class="error" for="name"></label>
												</div>
											</td>
										</tr>	
										<!-- 欄位：name -->
										<tr>
											<td class="header-require col-lg-2">標題</td>
											<td>
												<div class="col-lg-6 nopadding">											
													<input name="title" type="text" value="{{$message->title }}" readonly maxlength="50" id="title" class="form-control">											
													<label class="error" for="title"></label>
												</div>
											</td>
										</tr>									
										<!-- 欄位：desc -->
										<tr>
											<td class="header-require col-lg-2">內容</td>
											<td>
												<div class="col-lg-6 nopadding">											
												<textarea name="content" type="text" value="" readonly maxlength="200" id="content"  rows="10" class="form-control">{{$message->content}}</textarea>										
													<label class="error" for="content"></label>
												</div>
											</td>
										</tr>	
										 <!-- url -->
										 <tr>
											<td class="header-require col-lg-2">APP外連網址</td>
											<td>
												<div class="col-lg-6 nopadding">											
													<input name="url" type="text" value="{{$message->url}}" readonly maxlength="200" rows="2" id="url" class="form-control">											
													<label class="error" for="url"></label>
												</div>
											</td>
										</tr>
										<!-- 欄位：NewsID -->
										<tr>
											<td class="header-require col-lg-2">APP內連指定消息/活動連結</td>
											<td>
												<div class="col-lg-6 nopadding">											
													<div class="col-lg-6 nopadding">	
														{!! $BackendPresenter->getNewslist($message->news_id, 'disabled', 'create') !!}
													</div>
												</div>
											</td>
										</tr>	
										@if(!is_null($message->img_path))
										 <!-- img_path -->
										 <tr>
											<td class="header-require col-lg-2"><span style="color:red">*</span>圖片</td>
											<td>
												<div class="col-lg-4 nopadding">	
													<img src="{{ cdn_url('/message/' . $message->img_path) }}"  style="max-width: 600px;"/>
												</div>
											</td>
										</tr>	
										@endif		
										<!-- status -->
										<tr>
											<td class="header-require col-lg-2">已讀狀態</td>
											<td>
												<div class="col-lg-6 nopadding">	
													<label>{{($message->status)? '已讀':'未讀' }}</label>
												</div>
											</td>
										</tr>
										<!-- status -->
										<tr>
											<td class="header-require col-lg-2">建立日期</td>
											<td>
												<div class="col-lg-6 nopadding">	
													<label>{{$message->updated_at}}</label>
												</div>
											</td>
										</tr>									
										<!-- 下控制按鈕 -->
										<tr>
											<td>&nbsp;</td>
											<td>
												<div style="text-align: right">			
													<a class="btn btn-xs btn-default" href="{{ asset('/backend/message') }}">back</a>								
													{{-- <input type="submit" name="btnUpdate_foot" value="update" id="btnUpdate_foot" class="btn btn-primary btn-xs" onclick="submitForm();"> --}}
												</div>
											</td>
										</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- panel-body -->
				</div>
			</form>
		</div>
	</div>
@endsection

@section('extjs')
	<script>
	$(document).ready(function() 
	{

		//初始化需要偵錯的表格
		$('#EditForm').validate();
		//正規表達驗證初始化
		$.validator.addMethod(
			"regex",
			function (value, element, regexp) 
			{
				var re = new RegExp(regexp);
				return this.optional(element) || re.test(value);
			}
		);
		//各欄位
		$('#name').rules("add", 
		{
			required: true,
			minlength: 1,
			maxlength: 20,
			messages: {
				required: "name length must between 1-20",
				minlength: "name length must between 1-20",
				maxlength: "name length must between 1-20"
			}
		});
		
		//Update Mode
		$('#email').rules("add", 
		{
			required: true,
			email: true,
			minlength: 1,
			maxlength: 100,
			messages: {
				required: "Username length must between 1-100",
				email: "Username must be an email address",
				minlength: "Username length must between 1-100",
				maxlength: "Username length must between 1-100"
			}
		});
		
	});
	//提交與取消按鈕
	function submitForm() 
	{
		if (!!($("#EditForm").valid()) === false) 
		{
			return false;
		} 
		else 
		{
			$(document).ready(function() 
			{
				$.blockUI({ css: 
				{
					border: 'none',
					padding: '15px',
					backgroundColor: '#000',
					'-webkit-border-radius': '10px',
					'-moz-border-radius': '10px',
					opacity: .5,
					color: '#fff'
				}});
			});
		}
	}

	function cancelValidate() 
	{
		$("#EditForm").validate().cancelSubmit = true;
	}
	</script>
@endsection
