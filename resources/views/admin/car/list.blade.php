@extends('admin.master')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-dark">
				<div class="panel-heading">
					<div class="row">
						<div class="form-group">
							<div class="panel-title">Manage car</div>
						</div>
						<div class="form-inline">
                            <form method="post" action="{{ asset('/backend/car') }}" name="searchFrom">
								{{ csrf_field() }}   
								<div class="form-group" >
										<label class="control-label" style="color:white;">車主姓名 <br/>Name:</label><input type="text" name="name" value="{{ $cond->name }}"  class="form-control">
								</div>	                            
                                <div class="form-group" >
                                    <label class="control-label" style="color:white;">車輛號碼 <br/>VIN:</label><input type="text" name="vin" max="20" value="{{ $cond->vin }}"  class="form-control">
								</div>		
								<div class="form-group" >
									<label class="control-label" style="color:white;">車輛車牌 <br/>LicensePlateNumber:</label><input type="text" name="license_plate_number" value="{{ $cond->license_plate_number }}"  class="form-control">
								</div>
                                <div class="form-group" >
                                    <a id="btnOK" name="btnOK"  class="btn btn-danger btn-xs" >Search</a>									
                                </div>
                            </form>
                        </div>						
					</div>
				</div>
				<table class="table table-hover" cellspacing="0" width="100%">
					<thead>
						<tr>		
							<th>車主姓名</th>
							<th>VIN</th>
							<th>車牌號碼</th>							
							<th>車輛里程數</th>	
							<th>掛牌日</th>	
							<th>車款</th>		
							<th>車輛經銷商</th>						
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach($tables as $data)
							<tr>
								<td>{{ $data->name }}{{ $data->last_name }}</td>
								<td>{{ $data->vin }}</td>								
								<td>{{ $data->license_plate_number }}</td>
								<td>{{ $data->mileage }}</td>
								<td>{{ (is_null($data->member_listing_date)) ? '' : date('Y/m/d',strtotime($data->member_listing_date)) }}</td>
								<td>{{ $data->car_model }}</td>								
								<td>{{ $data->dealer }}</td>													
								<td style="text-align: right">
									
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				<div class="col-md-12 text-center no-margin">
					@if(count($tables)>0)
						<?php echo $tables->render(); ?>
					@endif
				</div>
			</div>
		</div>
	</div>

	<script>
        $(function () 
		{
            $("#btnOK").on("click", function () 
			{         
				if(checkFillInAtleastTwo())
				{      
					$('form[name="searchFrom"]').submit();     
				}           
            });
        });

		function checkFillInAtleastTwo() 
		{
			var name = $('input[name = name]').val().length;
			var vin = $('input[name = vin]').val().length;
			var lpn = $('input[name = license_plate_number]').val().length;

			if(name <= 0 && vin <= 0 && lpn <= 0)
			{
				alert('請至少輸入1個篩選條件');
				return false;
			}

			
			if(vin > 0 && vin < 5)
			{
				alert('VIN請至少輸入5碼');
				return false;
			} 

			return true;
			

		}

    </script>
@endsection
