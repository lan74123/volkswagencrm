@extends('admin.master')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-dark">
				<div class="panel-heading">
					<div class="row">
						<div class="col-md-6">
							<div class="panel-title"><i class="fas fa-users-cog"></i>使用者管理</div>
						</div>
						<div class="col-md-6" style="text-align: right">
							<a class="btn btn-darkblue btn-xs" href="{{ asset('/backend/user/create') }}"><i class="fas fa-plus"></i><strong>新增</strong></a>
						</div>
					</div>
				</div>
				<table class="table table-hover" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th>姓名</th>
							<th>Email</th>
							<th>建立時間</th>
							<th>最後修改</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach($users as $user)
							<tr>
								<td>{{ $user->id }}</td>
								<td>{{ $user->name }}</td>
								<td>{{ $user->email }}</td>
								<td>{{ $user->created_at }}</td>
								<td>{{ $user->updated_at }}</td>
								<td style="text-align: right">
									<form method="post" action="{{ asset('/backend/user/'.$user->id) }}">
										<a class="btn btn-success btn-xs" href="{{ asset('/backend/user/'.$user->id) }}">
											<i class="fas fa-edit"></i><strong>編輯</strong>
										</a>
										{{ csrf_field() }}
										{{ method_field('DELETE') }}
										<button type="submit" class="btn btn-danger btn-xs"><i class="fas fa-trash-alt"></i><strong>刪除</strong></button>
									</form>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
