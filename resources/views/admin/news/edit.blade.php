@extends('admin.master')

@section('content')
    @inject('BackendPresenter', 'App\Presenter\BackendPresenter')
    @if(Session::has('succUpdate'))
        {!! $BackendPresenter->showUpdateSuccDiv() !!}
    @endif
    @if(Session::has('previewClick'))
        <script>
           window.open("{{ asset('/press/newsroom/'.$datas->id) }}",'_blank');
        </script>
    @endif
    <div class="row">
        <div class="col-md-12">
            <form id="EditForm" class="form-horizontal" method="post" action="{{ asset('/backend/news/modify') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">編輯 Edit</h4>
                    </div>
                    <div class="panel-body">
                        <div>
                            <input name="id" type="readonly" value="{{ $news->id }}" id="id"  style="display:none">
                            <!-- 表格本體 -->
                            <table class="table" cellspacing="0" id="DetailsView1" style="border-collapse:collapse;">
                                <tbody>									
                                    <!-- 欄位：NewsType -->
                                    <tr>
                                        <td class="header-require col-lg-2"><span style="color:red">*</span>類別</td>
                                        <td>
                                            <div class="col-lg-10 nopadding">											
                                                <select id="type" name="type" class="form-control" disabled>
                                                    <option value="1" {{ ($news->type == "1") ? 'selected' : '' }}>消息 News</option>
                                                    <option value="2" {{ ($news->type == "2") ? 'selected' : '' }}>活動 Event</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- 欄位：NewsTitle -->
                                    <tr>
                                        <td class="header-require col-lg-2"><span style="color:red">*</span>名稱</td>
                                        <td>
                                            <div class="col-lg-10 nopadding">											
                                                <input name="title" type="text" value="{{ $news->title }}" maxlength="50" id="title" class="form-control">											
                                                <label class="error" for="title"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- 欄位：NewsShortDesc -->
                                    <tr>
                                        <td class="header-require col-lg-2"><span style="color:red">*</span>短描述</td>
                                        <td>
                                            <div class="col-lg-10 nopadding">			
                                                <textarea name="short_desc" type="text" value="" maxlength="200" id="short_desc"  rows="5" class="form-control">{{$news->short_desc}}</textarea>																	
                                                <label class="error" for="short_desc"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- 欄位：NewsDesc -->
                                    <tr>
                                        <td class="header-require col-lg-2"><span style="color:red">*</span>內容簡介</td>
                                        <td>
                                            <div class="col-lg-10 nopadding">		
                                                <textarea name="content_desc" type="text" value="" maxlength="500" id="content_desc"  rows="10" class="form-control">{{$news->content_desc}}</textarea>									
                                                <label class="error" for="content_desc"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- 欄位：Info -->
                                    <tr id="forEvent" style="display:none;">
                                        <td class="header-require col-lg-2"><span style="color:red">*</span>活動資訊</td>
                                        <td>
                                            <div class="col-lg-10 nopadding">
                                                <textarea name="info" type="text" value="" maxlength="300" id="info"  rows="5" class="form-control">{{$news->info}}</textarea>											
											    <label class="error" for="info"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- 欄位：NewsFile -->
                                    <tr>
                                        <td class="header-require col-lg-2"><span style="color:red">*</span>圖片上傳</td>
                                        <td>
                                            <div class="col-lg-10 nopadding">	
                                                <img src="{{ cdn_url('/news/' . $news->img_path) }}" width="100%"/>								
                                                <input type="file" name="img_path" class="multi with-preview maxsize-3072" accept="png|jpeg|jpg" />
                                                JPG格式 1400*1049，大小不得超過3MB / JPG 1400*1049 Size under 3MB
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- 欄位：NewsPublishDate -->
                                    <tr>
                                        <td class="header-require col-lg-2"><span style="color:red">*</span>上架日期</td>
                                        <td>
                                            <div class="col-lg-10 nopadding">
                                                <div class="input-group">											
                                                    <input name="publish_date" type="text" value="{{ $news->publish_date }}" id="publish_date" class="form-control">	
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>										
                                                    <label class="error" for="publish_date"></label>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- 欄位：NewsUrl -->
                                    <tr>
                                        <td class="header-require col-lg-2">外連網址</td>
                                        <td>
                                            <div class="col-lg-10 nopadding">											
                                                <input name="url" type="text" value="{{ $news->url }}" maxlength="200" rows="2" id="url" class="form-control">											
                                                <label class="error" for="url"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- 欄位：valid -->
                                    <tr>
                                        <td class="header-require col-lg-2">是否有效</td>
                                        <td>
                                            <div class="col-lg-5 nopadding">
                                                <input type="checkbox" name="valid" @if ($news->valid === 1) checked @endif>
                                                <label class="error" for="valid"></label>
                                            </div>
                                        </td>
                                    </tr>	
                                    <!-- 欄位：is_top -->
                                    <tr>
                                        <td class="header-require col-lg-2">是否置頂</td>
                                        <td>
                                            <div class="col-lg-5 nopadding">
                                                <input type="checkbox" name="is_top" @if ($news->is_top === 1) checked @endif>
                                                <label class="error" for="is_top"></label>
                                            </div>
                                        </td>
                                    </tr>	
                                    <tr>
                                        <td class="col-lg-2">建立時間</td>
                                        <td>{{ $news->created_at }}</td>
                                    </tr>
    
                                    <!-- Edit Mode -->
                                    <tr>
                                        <td class="col-lg-2">最後修改</td>
                                        <td>{{ $news->updated_at }}</td>
                                    </tr>								
                                    <!-- 下控制按鈕 -->
								    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <div style="text-align: right">		
                                                <a class="btn btn-xs btn-default" href="{{ asset('/backend/news') }}">back</a>									
                                                <input type="submit" name="btnUpdate_foot" value="update" id="btnUpdate_foot" class="btn btn-primary btn-xs" onclick="submitForm();">
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- panel-body -->
                </form>
                <!-- panel-footer -->
            </div>
        </div>
    </div>
@endsection

@section('extjs')
    <script>

        $(document).ready(function () {
            if ($("#type").val() == "2")
                $("[id*=forEvent]").removeAttr("style");
        });
        
        $("#type").change(function () {
            if ($("#type").val() == "2")
                $("[id*=forEvent]").removeAttr("style");
            else
                $("[id*=forEvent]").attr("style","display:none;");
        });
        
        $(function () {
            $("#publish_date").datepicker({ "dateFormat": "yy/mm/dd" });
        });
        
    </script>
@endsection
