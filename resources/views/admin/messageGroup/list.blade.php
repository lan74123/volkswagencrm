@extends('admin.master')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-dark">
				<div class="panel-heading">
					<div class="row">
						<div class="form-group">
							<div class="panel-title">Manage MessageGroup</div>
						</div>
						<div class="form-inline">
                            <form method="post" action="{{ asset('/backend/messageGroup') }}" name="searchFrom">
                                {{ csrf_field() }}                               
                                <div class="form-group" >
                                    <label class="control-label" style="color:white;">訊息群組名稱 <br/>MessageGroupName:</label><input type="text" name="name" value="{{ $cond->name }}"  class="form-control">
								</div>
								<div class="form-group" >
										<label class="control-label" style="color:white;">建立起始日期 <br/>Start Date:</label><input type="text" value="{{ $cond->date_start }}" id="date_start" name="date_start" class="form-control" autocomplete="off">
									</div>
								<div class="form-group" >
									<label class="control-label" style="color:white;">建立結束日期 <br/>End Date:</label><input type="text" value="{{ $cond->date_end }}" id="date_end" name="date_end" class="form-control" autocomplete="off">
								</div>																
                                <div class="form-group" >
                                    <a id="btnOK" name="btnOK"  class="btn btn-danger btn-xs" >Search</a>
									<a class="btn btn-darkblue btn-xs" href="{{ asset('backend/messageGroup/filterMemberList') }}"><strong>Add</strong></a>									
                                </div>
                            </form>
                        </div>						
					</div>
				</div>
				<table class="table table-hover" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>編號</th>
							<th>溝通群組名稱</th>
							<th>溝通群組描述</th>												
							<th>修改日期</th>	
							<th>建立日期</th>												
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach($tables as $data)
							<tr>
								<td>{{ $data->id }}</td>
								<td>{{ $data->name }}</td>
								<td>{{ $data->desc }}</td>	
								<td>{{ $data->updated_at }}</td>							
								<td>{{ $data->created_at }}</td>							
								<td style="text-align: right">
									<form method="post" action="{{ asset('/backend/messageGroup/delete/'.$data->id) }}">
										<a class="btn btn-warning btn-xs" href="{{ asset('/backend/messageGroup/edit/'.$data->id) }}">
											<strong>Edit</strong>
										</a>
										{{ csrf_field() }}
										{{ method_field('DELETE') }}
										<button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('即將永久刪除此筆訊息群組資料，確認繼續刪除?')"><strong>Delete</strong></button>
									</form>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				<div class="col-md-12 text-center no-margin">
						<?php echo $tables->render(); ?>
				</div>
			</div>
		</div>
	</div>

	<script>
        $(function () 
		{            
			$("#date_start").datepicker({ "dateFormat": "yy-mm-dd" });
            $("#date_end").datepicker({ "dateFormat": "yy-mm-dd" });

            $("#btnOK").on("click", function () 
			{         
				if(checkDate())
				{      
					$('form[name="searchFrom"]').submit();   
				}            
            });
        });

		//下載XLSX
		function downloadExcel(action) 
		{		 		
			$('form[name="searchFrom"]').attr("action", action);
			$('form[name="searchFrom"]').submit();
        };

		//判斷開始日期不得大於結束日期
        function  checkDate() 
		{
            var beginDate=$("#date_start").val();
            var endDate=$("#date_end").val();
            var d1 = new Date(beginDate.replace(/-/g, "/"));
            var d2 = new Date(endDate.replace(/-/g, "/"));

            if(beginDate!=""&&endDate!=""&&d1 >d2)
            {
                alert("開始日期不能晚於結束日期！\n The start date can not be later than the end date !");
                return false;
            }
			else
			{
                return true;
            }
        };

    </script>
@endsection
