@extends('admin.master')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<form id="EditForm" class="form-horizontal" method="post" action="{{ asset('/backend/messageGroup/modify') }}">
				{{ csrf_field() }}
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h4 class="panel-title">MessageGroup Edit</h4>
					</div>
					<div class="panel-body">
						<div>
							<input name="id" type="readonly" value="{{ $messageGroup->id }}" id="id"  style="display:none">
							<!-- 表格本體 -->
							<table class="table" cellspacing="0" id="DetailsView1" style="border-collapse:collapse;">
								<tbody>									
									<!-- 欄位：name -->
									<tr>
											<td class="header-require col-lg-2"><span style="color:red">*</span>溝通群組名稱</td>
											<td>
												<div class="col-lg-4 nopadding">											
													<input name="name" type="text" value="{{ $messageGroup->name }}" maxlength="50" id="name" class="form-control">											
													<label class="error" for="name"></label>
												</div>
											</td>
										</tr>										
										<!-- 欄位：desc -->
										<tr>
											<td class="header-require col-lg-2">溝通群組描述</td>
											<td>
												<div class="col-lg-4 nopadding">											
												<input name="desc" type="text" value="{{ $messageGroup->desc }}" maxlength="200" id="desc" class="form-control">											
													<label class="error" for="desc"></label>
												</div>
											</td>
										</tr>													
										<!-- 下控制按鈕 -->
										<tr>
											<td>&nbsp;</td>
											<td>
												<div style="text-align: right">			
													<a class="btn btn-xs btn-default" href="{{ asset('/backend/messageGroup') }}">back</a>								
													<input type="submit" name="btnUpdate_foot" value="update" id="btnUpdate_foot" class="btn btn-primary btn-xs" onclick="submitForm();">
												</div>
											</td>
										</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- panel-body -->
				</div>
			</form>
			
			@if(!empty($messageGroupMember))
				@if(count($messageGroupMember)>0)
					<div class="panel panel-primary">
						<div class="panel-heading">
								<h4 class="panel-title">MessageGroup MemberList</h4>
							</div>							
						<div class="panel-body">
								<form method="get" action="{{ asset('/backend/messageGroup/download/' . $messageGroup->id) }}">										
									{{ csrf_field() }}								
									<button type="submit" class="btn btn-info btn-xs" ><strong>Download List</strong></button>
								</form>							
							<table class="table table-hover" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>編號</th>
										<th>姓名&Email</th>
										<th>連絡電話</th>
										<th>連絡地址</th>
										<th>同意資料使用</th>							
										<th>註冊日期</th>																
										<th></th>
									</tr>
								</thead>
								<tbody>
									@foreach($messageGroupMember as $data)
										<tr>
											<td>{{ $data->member->id }}</td>
											<td>{{ $data->member->name }}<br>{{ $data->member->email }}</td>
											<td>{{ $data->member->mobile }}</td>								
											<td>{{ $data->member->address }}</td>
											<td>{{ ( $data->member->agree_data_use )? 'Y':'N' }}</td>
											<td>{{ $data->member->created_at }}</td>																	
											<td style="text-align: right">
												@if(count($messageGroupMember) > 1)
													<form method="post" action="{{ asset('/backend/messageGroup/deleteGroupMember/' . $messageGroup->id . '/' . $data->id) }}">										
														{{ csrf_field() }}
														{{ method_field('DELETE') }}
														<button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('即將永久刪除此筆資料，確認繼續刪除?')"><strong>Delete</strong></button>
													</form>	
												@endif								
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
							<div class="col-md-12 text-center no-margin">
									<?php echo $messageGroupMember->render(); ?>
							</div>
						</div>
					</div>
				@endif
			@endif
		</div>
	</div>
@endsection

@section('extjs')
	<script>
	$(document).ready(function() 
	{

		//初始化需要偵錯的表格
		$('#EditForm').validate();
		//正規表達驗證初始化
		$.validator.addMethod(
			"regex",
			function (value, element, regexp) 
			{
				var re = new RegExp(regexp);
				return this.optional(element) || re.test(value);
			}
		);
		//各欄位
		$('#name').rules("add", 
		{
			required: true,
			minlength: 1,
			maxlength: 20,
			messages: {
				required: "name length must between 1-20",
				minlength: "name length must between 1-20",
				maxlength: "name length must between 1-20"
			}
		});
		
		//Update Mode
		$('#email').rules("add", 
		{
			required: true,
			email: true,
			minlength: 1,
			maxlength: 100,
			messages: {
				required: "Username length must between 1-100",
				email: "Username must be an email address",
				minlength: "Username length must between 1-100",
				maxlength: "Username length must between 1-100"
			}
		});
		
	});
	//提交與取消按鈕
	function submitForm() 
	{
		if (!!($("#EditForm").valid()) === false) 
		{
			return false;
		} 
		else 
		{
			$(document).ready(function() 
			{
				$.blockUI({ css: 
				{
					border: 'none',
					padding: '15px',
					backgroundColor: '#000',
					'-webkit-border-radius': '10px',
					'-moz-border-radius': '10px',
					opacity: .5,
					color: '#fff'
				}});
			});
		}
	}

	function cancelValidate() 
	{
		$("#EditForm").validate().cancelSubmit = true;
	}
	</script>
@endsection
