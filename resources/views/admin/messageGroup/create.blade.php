@extends('admin.master')
@inject('BackendPresenter', 'App\Presenter\BackendPresenter')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<nav class="navbar navbar-default">
					<div class="container-fluid">
						<div class="navbar-header">
							<a class="navbar-brand" href="#">Create Flow</a>
						</div>
						<ul class="nav navbar-nav">
							<li><a href="#">Search Member List</a></li>
							<li><a href="#"> > </a></li>
							<li><a href="#">Add</a></li>
							<li><a href="#"> > </a></li>
							<li class="active"><a href="#">Group Info</a></li>
							<li><a href="#"> > </a></li>
							<li><a href="#">Create</a></li>
						</ul>
					</div>
				</nav>
			</div>
			<div class="panel panel-primary">			
				<form id="EditForm" class="form-horizontal" method="post" action="{{ asset('/backend/messageGroup/insert') }}">
					{{ csrf_field() }}
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h4 class="panel-title">MessageGroup Add</h4>
						</div>
						<div class="panel-body">
							<div>
								<!-- 表格本體 -->
								<table class="table" cellspacing="0" id="DetailsView1" style="border-collapse:collapse;">
									<tbody>
										<!-- 欄位：memberFilter -->
										<input name="filter_name" type="hidden" value="{{$filterCond['name']}}" class="form-control">
										<input name="filter_mobile" type="hidden" value="{{$filterCond['mobile']}}" class="form-control">
										<input name="filter_car_model" type="hidden" value="{{$filterCond['car_model']}}" class="form-control">
										<input name="filter_last_maintenance" type="hidden" value="{{$filterCond['last_maintenance']}}" class="form-control">
										<input name="filter_mileage" type="hidden" value="{{$filterCond['mileage']}}" class="form-control">
										<input name="filter_date_start" type="hidden" value="{{$filterCond['date_start']}}" class="form-control">
										<input name="filter_date_end" type="hidden" value="{{$filterCond['date_end']}}" class="form-control">
										<!-- 欄位：name -->
										<tr>
											<td class="header-require col-lg-2"><span style="color:red">*</span>溝通群組名稱</td>
											<td>
												<div class="col-lg-4 nopadding">											
													<input name="name" type="text" value="{{old('name')}}" maxlength="50" id="name" class="form-control">											
													<label class="error" for="name"></label>
												</div>
											</td>
										</tr>										
										<!-- 欄位：desc -->
										<tr>
											<td class="header-require col-lg-2">溝通群組描述</td>
											<td>
												<div class="col-lg-4 nopadding">											
												<input name="desc" type="text" value="{{old('desc')}}" maxlength="200" id="desc" class="form-control">											
													<label class="error" for="desc"></label>
												</div>
											</td>
										</tr>													
										<!-- 下控制按鈕 -->
										<tr>
											<td>&nbsp;</td>
											<td>
												<div style="text-align: right">			
													<a class="btn btn-xs btn-default" href="{{ asset('/backend/messageGroup') }}">cancle</a>								
													<input type="submit" name="btnUpdate_foot" value="create" id="btnUpdate_foot" class="btn btn-primary btn-xs" onclick="submitForm();">
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<!-- panel-body -->					
					</div>
				</form>		
			</div>		
	</div>
@endsection

@section('extjs')
	<script>
	$(document).ready(function() 
	{		
		//初始化需要偵錯的表格
		$('#EditForm').validate();
		//正規表達驗證初始化
		$.validator.addMethod(
			"regex",
			function (value, element, regexp) 
			{
				var re = new RegExp(regexp);
				return this.optional(element) || re.test(value);
			}
		);
		//各欄位
		$('#name').rules("add", 
		{
			required: true,
			minlength: 1,
			maxlength: 20,
			messages: {
				required: "name length must between 1-20",
				minlength: "name length must between 1-20",
				maxlength: "name length must between 1-20"
			}
		});		
	});
	//提交與取消按鈕
	function submitForm() 
	{
		if (!!($("#EditForm").valid()) === false) 
		{
			return false;
		} 
		else 
		{
			$(document).ready(function() 
			{
				$.blockUI({ css: {
					border: 'none',
					padding: '15px',
					backgroundColor: '#000',
					'-webkit-border-radius': '10px',
					'-moz-border-radius': '10px',
					opacity: .5,
					color: '#fff'
				}});
			});
		}
	}

	function cancelValidate() 
	{
		$("#EditForm").validate().cancelSubmit = true;
	}
	</script>
@endsection
