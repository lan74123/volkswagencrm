@extends('admin.master')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-dark">
				<div class="panel-heading">
					<div class="row">
						<div class="col-md-6">
							<div class="panel-title"><i class="fas fa-bullhorn"></i>活動管理</div>
						</div>
						<div class="col-md-6" style="text-align: right">
							<a class="btn btn-darkblue btn-xs" href="{{ asset('/backend/campaign/create') }}"><i class="fas fa-plus"></i><strong>新增</strong></a>
						</div>
					</div>
				</div>
				<table class="table table-hover" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th>種類</th>
							<th>名稱</th>
							<th>地點</th>
							<th>活動區間</th>
							<th>有無效</th>
							<th>建立時間</th>
							<th>最後修改</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach($campaigns as $campaign)
							<tr>
								<td>{{ $campaign->id }}</td>
								<td>{{ $campaign->type }}</td>
								<td>{{ $campaign->name }}</td>
								<td>{{ $campaign->place }}</td>
								<td>{{ toDate($campaign->start_date) }} ~ {{ toDate($campaign->end_date) }}</td>
								<td>
									<div class="nopadding nomargin checkbox checkbox-primary">
										<input type="checkbox" @if ($campaign->valid === 1) checked @endif disabled>
										<label></label>
									</div>
								</td>
								<td>{{ $campaign->created_at }}</td>
								<td>{{ $campaign->updated_at }}</td>
								<td style="text-align: right">
									<form method="post" action="{{ asset('/backend/campaign/' . $campaign->id) }}">
										<a class="btn btn-success btn-xs" href="{{ asset('/backend/campaign/' . $campaign->id) }}">
											<i class="fas fa-edit"></i><strong>編輯</strong>
										</a>
										{{ csrf_field() }}
										{{ method_field('DELETE') }}
										<button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('此動作無法回復，確認刪除?')"><i class="fas fa-trash-alt"></i><strong>刪除</strong></button>
									</form>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
