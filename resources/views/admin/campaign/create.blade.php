@extends('admin.master')

@inject('BackendPresenter', 'App\Presenter\BackendPresenter')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<form id="EditForm" class="form-horizontal" method="post" action="{{ asset('/backend/campaign/store') }}" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title">新增活動</h4>
				</div>
				<div class="panel-body">
					<div>
						<table class="table" cellspacing="0" id="DetailsView1" style="border-collapse:collapse;">
							<tbody>
								<tr>
									<td class="header-require col-lg-2">種類</td>
									<td>
										<div class="col-lg-3 nopadding">
											<select name="type" id="type" class="form-control">
												<option>請選擇</option>
												<option value="一般活動" @if (old('type') == '一般活動') selected @endif>一般活動</option>
												<option value="會員限定" @if (old('type') == '會員限定') selected @endif>會員限定</option>
											</select>
											<label class="error" for="type"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td class="header-require col-lg-2">名稱</td>
									<td>
										<div class="col-lg-5 nopadding">
											<input name="name" type="text" maxlength="50" id="name" class="form-control" value="{{ old('name') }}">
											<label class="error" for="name"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td class="header-require col-lg-2">地點</td>
									<td>
										<div class="col-lg-5 nopadding">
											<input name="place" type="text" maxlength="100" id="place" class="form-control" value="{{ old('place') }}">
											<label class="error" for="place"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td class="header-require col-lg-2">圖片</td>
									<td>
										<div class="col-lg-3 nopadding">
											<input type="file" name="image" id="image" />
										</div>
									</td>
								</tr>
								<tr>
									<td class="header-require col-lg-2">活動起</td>
									<td>
										<div class="col-lg-3 nopadding">
											<input name="start_date" type="date" id="start_date" class="form-control" value="{{ old('start_date') }}">
											<label class="error" for="start_date"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td class="header-require col-lg-2">活動迄</td>
									<td>
										<div class="col-lg-3 nopadding">
											<input name="end_date"" type="date" id="end_date" class="form-control" value="{{ old('end_date') }}">
											<label class="error" for="end_date"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td class="header-require col-lg-2">活動描述</td>
									<td>
										<div class="col-lg-5 nopadding">
											<textarea name="description" id="description" rows="20" class="form-control">{{ old('description') }}</textarea>
										</div>
									</td>
								</tr>
								<tr>
									<td class="header-require col-lg-2">有無效</td>
									<td>
										<div class="col-lg-3 nopadding checkbox checkbox-primary">
											<input name="valid" type="checkbox" id="valid" checked>
											<label></label>
											<label class="error" for="valid"></label>
										</div>
									</td>
								</tr>
								<!-- 下控制按鈕 -->
								<tr>
									<td>&nbsp;</td>
									<td>
										<div style="text-align: right">
											<input type="submit" name="btnUpdate_foot" value="儲存" id="btnUpdate_foot" class="btn btn-primary btn-xs" onclick="submitForm();">
											<input type="button" name="btnBackTo2_foot" value="返回" id="btnBackTo2_foot" class="btn btn-default btn-xs">
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<!-- panel-body -->
			</div>
		</form>
	</div>
</div>
@endsection

@section('extjs')
<script>
	
	$(document).ready(function () {
		//Back
		$("#btnBackTo2_foot").click(function () {
			location.href = '{{ asset('backend/campaign') }}';
		});
	});
	//提交與取消按鈕
	function submitForm() {
		if (!!($("#EditForm").valid()) === false) {
			return false;
		} else {
			$(document).ready(function () {
				$.blockUI({
					css: {
						border: 'none',
						padding: '15px',
						backgroundColor: '#000',
						'-webkit-border-radius': '10px',
						'-moz-border-radius': '10px',
						opacity: .5,
						color: '#fff'
					}
				});
			});
		}
	}
</script>
@endsection