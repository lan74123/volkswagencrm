@extends('admin.master')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<form id="EditForm" class="form-horizontal" method="post" action="{{ asset('/backend/member/insert') }}">
				{{ csrf_field() }}
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h4 class="panel-title">Member Add</h4>
					</div>
					<div class="panel-body">
						<div>
							<!-- 表格本體 -->
							<table class="table" cellspacing="0" id="DetailsView1" style="border-collapse:collapse;">
								<tbody>									
									<!-- 欄位：MemberName -->
									<tr>
										<td class="header-require col-lg-2"><span style="color:red">*</span>姓名</td>
										<td>
											<div class="col-lg-4 nopadding">											
												<input name="name" type="text" value="" maxlength="20" id="name" class="form-control">											
												<label class="error" for="name"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：UserEmail -->								
									<tr>
										<td class="header-require col-lg-2"><span style="color:red">*</span>帳號</td>
										<td>
											<div class="col-lg-4 nopadding">
												<input name="email" type="text" value="" maxlength="100" id="email" class="form-control">
												<label class="error" for="email"></label>
											</div>
										</td>
									</tr>									
									<!-- 欄位：Password -->
									<tr>
										<td class="header-require col-lg-2"><span style="color:red">*</span>密碼</td>
										<td>
											<div class="col-lg-4 nopadding">
												<input name="password" type="password" value="" maxlength="20" id="password" class="form-control">
												<label class="error" for="password"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：mobile -->
									<tr>
										<td class="header-require col-lg-2">連絡電話</td>
										<td>
											<div class="col-lg-4 nopadding">
												<input name="mobile" type="text" value="" maxlength="20" id="mobile" class="form-control">
												<label class="error" for="mobile"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：address -->
									<tr>
										<td class="header-require col-lg-2">連絡地址</td>
										<td>
											<div class="col-lg-5 nopadding">
												<input name="address" type="text" value="" maxlength="20" id="address" class="form-control">
												<label class="error" for="address"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：agree_data_use -->
									<tr>
										<td class="header-require col-lg-2">同意資料使用</td>
										<td>
											<div class="col-lg-5 nopadding">
												<input type="checkbox" name="agree_data_use" checked>
												<label class="error" for="agree_data_use"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：agree_data_use -->
									<tr>
										<td class="header-require col-lg-2">是否有效</td>
										<td>
											<div class="col-lg-5 nopadding">
												<input type="checkbox" name="valid" checked>
												<label class="error" for="valid"></label>
											</div>
										</td>
									</tr>									
									<!-- 下控制按鈕 -->
									<tr>
										<td>&nbsp;</td>
										<td>
											<div style="text-align: right">			
												<a class="btn btn-xs btn-default" href="{{ asset('/backend/member') }}">back</a>								
												<input type="submit" name="btnUpdate_foot" value="create" id="btnUpdate_foot" class="btn btn-primary btn-xs" onclick="submitForm();">
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- panel-body -->
				</div>
			</form>
		</div>
	</div>
@endsection

@section('extjs')
	<script>
	$(document).ready(function() 
	{		
		//初始化需要偵錯的表格
		$('#EditForm').validate();
		//正規表達驗證初始化
		$.validator.addMethod(
			"regex",
			function (value, element, regexp) 
			{
				var re = new RegExp(regexp);
				return this.optional(element) || re.test(value);
			}
		);
		//各欄位
		$('#name').rules("add", 
		{
			required: true,
			minlength: 1,
			maxlength: 20,
			messages: {
				required: "name length must between 1-20",
				minlength: "name length must between 1-20",
				maxlength: "name length must between 1-20"
			}
		});
		
		//Insert Mode
		$('#email').rules("add", 
		{
			required: true,
			email: true,
			minlength: 1,
			maxlength: 100,
			messages: {
				required: "Username length must between 1-100",
				email: "Username must be an email address",
				minlength: "Username length must between 1-100",
				maxlength: "Username length must between 1-100"
			}
		});
	
		//Insert Mode
		$('#password').rules("add", 
		{
			required: true,
			minlength: 1,
			maxlength: 20,
			messages: {
				required: "Password length must between 1-20",
				minlength: "Password length must between 1-20",
				maxlength: "Password length must between 1-20"
			}
		});
		
		
	});
	//提交與取消按鈕
	function submitForm() 
	{
		if (!!($("#EditForm").valid()) === false) 
		{
			return false;
		} 
		else 
		{
			$(document).ready(function() 
			{
				$.blockUI({ css: {
					border: 'none',
					padding: '15px',
					backgroundColor: '#000',
					'-webkit-border-radius': '10px',
					'-moz-border-radius': '10px',
					opacity: .5,
					color: '#fff'
				}});
			});
		}
	}

	function cancelValidate() 
	{
		$("#EditForm").validate().cancelSubmit = true;
	}
	</script>
@endsection
