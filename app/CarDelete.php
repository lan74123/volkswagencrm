<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarDelete extends Model
{
    public function member()
    {
        return $this->hasOne('App\Member', 'id', 'member_id');
    }
    
    public function car_model()
    {
        return $this->hasOne('App\CarModel', 'id', 'car_id');
    }
}
