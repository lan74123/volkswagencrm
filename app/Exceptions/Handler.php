<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // 參數驗證錯誤的異常，我們需要返回 400 的 http code 和一句錯誤信息
        if ($exception instanceof ValidationException) 
        {   
            $fullUrl = $request->fullUrl();

            if (strpos($fullUrl, '/backend/') !== false) 
            {
                return parent::render($request, $exception);
            }
            else
            {
                return response(['error' => array_first(array_collapse($exception->errors()))], 400);
            }
        }
        // 用户認證的異常，我們需要返回 401 的 http code 和錯誤信息
        if ($exception instanceof UnauthorizedHttpException) 
        {
            return response($exception->getMessage(), 401);
        }
        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {  
        
        $guard = array_get($exception->guards(),0);
       
        if($guard == "api")
        {
            return response()->json([
                    'Result' => 'N',
                    'ErrorCode' => '',
                    'ErrorMsg' => 'token 無效',
                    'UserToken' => '',
            ], 200)->withHeaders([
                    'Content-Type' => 'application/json',
                    'Access-Control-Allow-Origin' => '*',
            ]);
        }

        if ($request->expectsJson()) 
        {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('login'));
    }
}
