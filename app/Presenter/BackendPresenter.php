<?php
namespace App\Presenter;

use App\Repository as Repo;

class BackendPresenter
{
    protected $user;

    /**
     * 建構子
     *
     * @return void
     */
    public function __construct(Repo\Backend\UserRepository $user, 
    Repo\Backend\DealerRepository $dealer,
    Repo\Backend\MessageGroupRepository $messageGroup,
    Repo\Common\NewsRepository $news)
    {
        $this->user = $user;
        $this->dealer = $dealer;
        $this->messageGroup = $messageGroup;
        $this->news = $news;
    }

    public function showUpdateSuccDiv()
    {
        $compoment = '<div class="alert alert-success">異動成功</div>';

        return $compoment;
    }
    
    /**
     * 初始區域
     *
     * @param [type] $allplace
     * @return void
     */
    public function getAreaList($allplace)
    {
        $areaList = array();

        if ($allplace)
        {
            $areaList[''] = '不拘';
        }

        $areaList['大台北地區'] = '大台北地區';
        $areaList['中彰投地區'] = '中彰投地區';
        $areaList['宜花東地區'] = '宜花東地區';
        $areaList['桃竹苗地區'] = '桃竹苗地區';
        $areaList['高屏地區'] = '高屏地區';
        $areaList['雲嘉南地區'] = '雲嘉南地區';

        return $areaList;
    } 

    /**
     * 區域列表
     *
     * @param [type] $area
     * @param [type] $disabled
     * @param [type] $allplace
     * @return void
     */
    public function getAreaSelectlist($area, $disabled, $allplace)
    {
        $compoment = '';
        $areaList = $this->getAreaList($allplace);

        if($disabled == 'disabled')
        {
            $compoment = '<select id="area" name="area" class="form-control" disabled="disabled">';
        }
        else
        {
            $compoment = '<select id="area" name="area" class="form-control">';
        }

        $i=0 ;

        if(empty($area)) 
        {
            foreach ($areaList as $key => $value) 
            {
                if ($i < 1) 
                {
                    $compoment = $compoment . '<option value="' . $key . '" selected>' . $value . '</option>';
                } 
                else 
                {
                    $compoment = $compoment . '<option value="' . $key . '" >' . $value . '</option>';
                }
                $i++;
            }
        }
        else
        {
            foreach ($areaList as $key => $value) 
            {
                if ($key == $area) 
                {
                    $compoment = $compoment . '<option value="' . $key . '" selected>' . $value . '</option>';
                } 
                else 
                {
                    $compoment = $compoment . '<option value="' . $key . '" >' . $value . '</option>';
                }
            }
        }

        $compoment = $compoment . '</select>';

        return $compoment;
    }

    /**
     * 經銷商列表
     *
     * @param [type] $dealer
     * @param [type] $disabled
     * @param [type] $allplace
     * @return void
     */
    public function getDealerSelectlist($dealer, $disabled, $allplace)
    {
        $compoment = '';
        $dealerList = $this->dealer->searchDealers(0, null, null, null, '1');

        if($disabled == 'disabled')
        {
            $compoment = '<select id="dealer" name="dealer" class="form-control" disabled="disabled">';
        }
        else
        {
            $compoment = '<select id="dealer" name="dealer" class="form-control">';
        }

        if(!$allplace)
        {
            $compoment = $compoment .'<option value="" >不拘</option>';
        }        
     
        $i=0 ;

        if(empty($dealer)) 
        {
            foreach ($dealerList as $key) 
            {
                if ($i < 1) 
                {
                    $compoment = $compoment . '<option value="' . $key->id . '">' . $key->name . '</option>';
                } 
                else 
                {
                    $compoment = $compoment . '<option value="' . $key->id . '" >' . $key->name . '</option>';
                }
                $i++;
            }
        }
        else
        {
            foreach ($dealerList as $key) 
            {
                if ($key->id == $dealer) 
                {
                    $compoment = $compoment . '<option value="' . $key->id . '" selected>' . $key->name . '</option>';
                } 
                else 
                {
                    $compoment = $compoment . '<option value="' . $key->id . '" >' . $key->name . '</option>';
                }
            }
        }

        $compoment = $compoment . '</select>';

        return $compoment;
    }

     /**
     * 初始服務類型
     *
     * @param [type] $allplace
     * @return void
     */
    public function getServiceList($allplace)
    {
        $serviceList= array();

        if($allplace)
        {
            $serviceList[''] = '不拘';
        }

        $serviceList['S'] = '展示中心';
        $serviceList['M'] = '服務廠';

        return $serviceList;
    } 

    /**
     * 服務類型列表      
     *
     * @param [type] $type
     * @param [type] $disabled
     * @param [type] $allplace
     * @return void
     */
    public function getServiceSelectlist($type, $disabled, $allplace)
    {
        $compoment = '';
        
        $serviceList = $this->getServiceList($allplace);

        if($disabled == 'disabled')
        {
            $compoment = '<select id="type" name="type" class="form-control" disabled="disabled">';
        }
        else
        {
            $compoment = '<select id="type" name="type" class="form-control">';
        }

        $i=0 ;

        if(empty($type)) 
        {
            foreach ($serviceList as $key => $value) 
            {
                if ($i < 1) 
                {
                    $compoment = $compoment . '<option value="' . $key . '" selected>' . $value . '</option>';
                } 
                else 
                {
                    $compoment = $compoment . '<option value="' . $key . '" >' . $value . '</option>';
                }
                $i++;
            }
        }
        else
        {
            foreach ($serviceList as $key => $value) 
            {
                if ($key == $type) 
                {
                    $compoment = $compoment . '<option value="' . $key . '" selected>' . $value . '</option>';
                } 
                else 
                {
                    $compoment = $compoment . '<option value="' . $key . '" >' . $value . '</option>';
                }
            }
        }

        $compoment = $compoment . '</select>';

        return $compoment;
    }

    /**
     * 取得服務類型名稱
     *
     * @param [type] $type
     * @return void
     */
    public function getServiceName($type, $allplace)
    {
        $serviceList = $this->getServiceList($allplace);

        return $serviceList[$type];
    }
   

     /**
     * 初始訊息類型
     *
     * @param [type] $allplace
     * @return void
     */
    public function getMessageList()
    {
        $messageList= array();      

        $messageList['1'] = 'EMAIL';
        $messageList['2'] = 'PUSH';
        $messageList['3'] = 'SMS';

        return $messageList;
    } 

    /**
     * 訊息類型列表  
     *
     * @param [type] $name checkbox name
     * @param string $types ex 1,2,3,
     * @return void
     */
    public function getMessageCheckBoxList($name = null , $types = '')
    {
        $compoment = '';
        
        $messageList = $this->getMessageList();

        if(!is_array($types))
        {
            $typeList = explode(',', $types);
        }
        else
        {
            $typeList = $types;
        }       

        if(count($typeList) > 0) 
        {
            foreach ($messageList as $key => $value) 
            {
                $checked = false;

                foreach ($typeList as $type) 
                {
                    if($key == $type)
                    {
                        $checked = true ;
                    }
                }

                if($checked)
                {
                    $compoment = $compoment . '<input name="' . $name . '[]" type="radio" value="' . $key . '" checked /><label for="' . $value . '">' . $value . '</label>';
                }
                else
                {
                    $compoment = $compoment . '<input name="' . $name . '[]" type="radio" value="' . $key . '" /><label for="' . $value . '">' . $value . '</label>';
                }               
            }
        }
        else
        {
            foreach ($messageList as $key => $value) 
            {
                $compoment = $compoment . '<input name="' . $name . '[]" type="radio" value="' . $key . '" /><label for="' . $value . '">' . $value . '</label>';
            }
        }        

        return $compoment;
    }

    /**
     * 訊息群組列表  
     *
     * @param [type] $dealer
     * @param [type] $disabled
     * @param [type] $allplace
     * @return void
     */
    public function getMessageGrouplist($messageGroup, $disabled, $allplace)
    {
        $compoment = '';
        $messageGroupList = $this->messageGroup->searchMessageGroups(0, '', '', '', 1);

        if($disabled == 'disabled')
        {
            $compoment = '<select id="message_groups_id" name="message_groups_id" class="form-control" disabled="disabled">';
        }
        else
        {
            $compoment = '<select id="message_groups_id" name="message_groups_id" class="form-control">';
        }     
     
        $i=0 ;

        if(empty($messageGroupList)) 
        {
            foreach ($messageGroupList as $key) 
            {
                if ($i < 1) 
                {
                    $compoment = $compoment . '<option value="' . $key->id . '">' . $key->name . '</option>';
                } 
                else 
                {
                    $compoment = $compoment . '<option value="' . $key->id . '" >' . $key->name . '</option>';
                }
                $i++;
            }
        }
        else
        {
            foreach ($messageGroupList as $key) 
            {
                if ($key->id == $messageGroup) 
                {
                    $compoment = $compoment . '<option value="' . $key->id . '" selected>' . $key->name . '</option>';
                } 
                else 
                {
                    $compoment = $compoment . '<option value="' . $key->id . '" >' . $key->name . '</option>';
                }
            }
        }

        $compoment = $compoment . '</select>';

        return $compoment;
    }

    /**
     * 消息活動列表  
     *
     * @param [type] $disabled
     *
     * @return void
     */
    public function getNewslist($news, $disabled, $action)
    {
        $compoment = '';
        $newsList = $this->news->searchPublicNews(0, null, null);

        if($disabled == 'disabled')
        {
            $compoment = '<select id="news_id" name="news_id" class="form-control" disabled="disabled">';
        }
        else
        {
            $compoment = '<select id="news_id" name="news_id" class="form-control">';
        }     
     
        $i=0 ;

        if(empty($newsList)) 
        {
            if($action == 'create')
            {
                $compoment = $compoment . '<option value="0">無</option>';
            }

            foreach ($newsList as $key) 
            {
                if ($i < 1) 
                {
                    $compoment = $compoment . '<option value="' . $key->id . '">' . $key->title . '</option>';
                } 
                else 
                {
                    $compoment = $compoment . '<option value="' . $key->id . '" >' . $key->title . '</option>';
                }
                $i++;
            }
        }
        else
        {
            if($action == 'create')
            {
                $compoment = $compoment . '<option value="0">無</option>';
            }
            
            foreach ($newsList as $key) 
            {
                if ($key->id == $news) 
                {
                    $compoment = $compoment . '<option value="' . $key->id . '" selected>' . $key->title . '</option>';
                } 
                else 
                {
                    $compoment = $compoment . '<option value="' . $key->id . '" >' . $key->title . '</option>';
                }
            }
        }

        $compoment = $compoment . '</select>';

        return $compoment;
    }
    
}