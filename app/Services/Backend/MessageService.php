<?php
namespace App\Services\Backend;


use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Repository as Repo;
use App\Presenter\MessagePresenter;
use App;
use Cookie;


class MessageService
{
    //訊息Service
    protected $message, $messageGroup;

    /**
     * 建構子
     *
     * @param Repo\Backend\Message $messagesitory
     */
    public function __construct(Repo\Backend\MessageRepository $message,
    Repo\Backend\MessageGroupRepository $messageGroup)
    {
        $this->message = $message;
        $this->messageGroup = $messageGroup;
    }

    /**
     * 搜尋多筆訊息
     *
     * @param Request $request
     * @param integer $pageLimit
     * @return void
     */
    function searchMessage(Request $request, $pageLimit = 0)
    {
        if ( $_SERVER['REQUEST_METHOD'] == "GET" )
        {
            $request->name = ($request->cookie('b_msg_name')) ? $request->cookie('b_msg_name') : $request->name;         
            $isValidNull = is_null($request->cookie('b_msg_valid'));
            $request->date_start = ($request->cookie('b_msg_date_start')) ? $request->cookie('b_msg_date_start') : $request->date_start;
            $request->date_end= ($request->cookie('b_msg_date_end')) ? $request->cookie('b_msg_date_end') : $request->date_end;
        }
        else
        {
            Cookie::queue('b_msg_name', $request->name, 60);     
            Cookie::queue('b_msg_date_start', $request->date_start, 60);
            Cookie::queue('b_msg_date_end', $request->date_end, 60);           
        }

        $name = $request->name; 
        $date_start = $request->date_start;
        $date_end = $request->date_end;
        $valid = $request->valid;
       
        return $this->message->searchMessages($pageLimit, $name, $date_start, $date_end, $valid);
    }

    /**
     * 取得單筆訊息
     *
     * @param integer $id
     * @return void
     */
    function getMessage($id = 0)
    {
        return $this->message->getMessage($id);
    }

    /**
     * 新增訊息
     *
     * @param Request $request
     * @return void
     */
    function insertMessage(Request $request)
    {
       
        $validateRules = [            
            'title' => 'required|max:50',
            'content' => 'required|max:200',
            'img_path' => 'image|mimes:jpeg,png,bmp',
            'sendTimeType' => 'required',
            'url' => 'max:200',  
        ];
        
        $validateMessage = [
            'title.required' => MessagePresenter::getRequired('title.required'),
            'title.max' => MessagePresenter::getMax('title.max', 50),
            'content.required' => MessagePresenter::getRequired('content.required'),
            'content.max' => MessagePresenter::getMax('content.max', 200),           
            'img_path.image' => MessagePresenter::getImage('圖片'),
            'img_path.mimes' => MessagePresenter::getMimes('圖片', 'JPG, PNG, BMP'),
            'sendTimeType.required' => MessagePresenter::getRequired('sendTimeType','設定日期及時間'),
            'url.max' => MessagePresenter::getMax('url.max', 200),
        ];

        $request->validate($validateRules, $validateMessage);
        
        $messageGroupMemberList = $this->messageGroup->getMessageGroupMember(0, $request->message_groups_id);

        $img_name = date('YmdHis').'';
        $img_path_url = '';
        foreach($messageGroupMemberList as $messageMember)
        {
            $request->member_id = $messageMember->member_id; 

            if(!empty($img_path_url))
            {
                $request->img_path_url = $img_path_url;
            }   
                       
            $img_path_url = $this->message->insertMessage($request, $img_name);

        }
    }    

    function deleteMessage($id = 0)
    {
        $this->message->deleteMessage($id);
    }

    /**
     * 搜尋訊息By MEMBER_ID
     *
     * @param Request $request
     * @param integer $pageLimit
     * @return void
     */
    function getMessageByMember($pageLimit = 0, $member_id = 0)
    {   
        $message_all = $this->message->getMessageByMember($pageLimit, $member_id);
        foreach($message_all as $message)
        {
            if(!is_null($message->img_path))
            {
                if($message->img_path != '')
                {
                    $message->img_path = asset('storage/message/' . $message->img_path);
                }
            }
        }

        return $message_all;
    }

    /**
     * 搜尋訊息By ID 更新已讀狀況
     *
     * @param integer $id
     * @param integer $member_id
     * @return void
     */
    function findMessageByID($id = 0, $member_id = 0)
    {   
        $message = $this->message->findMessageByID($id, $member_id);
        if(!is_null($message->img_path))
        {
            if($message->img_path != '')
            {
                $message->img_path = asset('storage/message/' . $message->img_path);
            }
        }
        return $message;
    }

    /**
     * 發送推播
     *
     * @return void
     */
    function sendMessage()
    {   
        $message = $this->message->findPreSendMessage(date('Y-m-d H:i:s'));

        foreach($message as  $send_message)
        {
            if(!is_null($send_message->push_token))
            {
                //PHP treats NULL, false, 0, and the empty string as equal.
                if($send_message->push_token != '')
                { 
                    $this->FCMPush($send_message);  
                }               
            }
            
        }
    }

     /**
     * 發送推播ByID
     *
     * @return void
     */
    function findPreSendMessageById($id = 0 )
    {   
        $message = $this->message->findPreSendMessageById($id);

        foreach($message as  $send_message)
        {
            if(!is_null($send_message->push_token))
            {
                //PHP treats NULL, false, 0, and the empty string as equal.
                if($send_message->push_token != '')
                { 
                    $this->FCMPush($send_message);  
                }               
            }
            
        }
    }

    public function FCMPush($send_message) 
    {
        try
        {
            $token 	 = $send_message->push_token;/*FCM 接收端的token*/
            $message = $send_message->content;/*要接收的內容*/
            $title 	 = $send_message->title;  /*要接收的標題*/
            $news_id = ($send_message->news_id == '0') ? '' : $send_message->news_id;/* 內連指定消息/活動 */
    
            if($send_message->device == 'ios')
            {

            }
            else  if($send_message->device == 'android')
            {

            }

            /*
            notification組成格式 官方範例
            {
            "message":{
                "token":"bk3RNwTe3H0:CI2k_HHwgIpoDKCIZvvDMExUdFQ3P1...",
                "notification":{
                "title":"Portugal vs. Denmark",
                "body":"great match!"
                }
            }
            }
            */
            
            $content = array
            (
                'title'	=> $title,
                'body' 	=> $message,
                'sound' => 'default'          
            );

            $data = array
            (
                'news_id'	=> $news_id       
            );

            $fields = array
            (
                'to'		    => $token,
                'notification'	=> $content,
                'data'          => $data
            );
            
            //firebase認證 與 傳送格式
            $headers = array
            (
                'Authorization: key='. env('FCM_API_ACCESS_KEY'),
                'Content-Type: application/json'
            );
            
            /*curl至firebase server發送到接收端*/
            $ch = curl_init();//建立CURL連線
            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec($ch );        
            //result 是firebase server的結果   
            
            curl_close( $ch );//關閉CURL連線

            $result_array = (array) json_decode($result);

            if($result_array['success'] == '1')
            { 
                $this->message->sendMessageSuccess($send_message->id);
                
            }   
        
        }
        catch(\Throwable $e)
        {
            
        }       
    }
}