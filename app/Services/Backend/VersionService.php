<?php

namespace App\Services\Backend;

use Illuminate\Http\Request;
use App\Repository as Repo;
use App\Presenter\MessagePresenter;
use App;
use Cookie;

class VersionService
{
    //Version Service
    protected $version;

    /**
     * 建構子
     *
     * @param Repo\Backend\Version $version
     */
    public function __construct(Repo\Backend\VersionRepository $version)
    {
        $this->version = $version;
    }

    /**
     * 搜尋多筆經銷商
     *
     * @param Request $request
     * @param integer $pageLimit
     * @return void
     */
    function searchVersion(Request $request, $pageLimit = 0)
    {
        if ( $_SERVER['REQUEST_METHOD'] == "GET" )
        {          
            $request->version = ($request->cookie('b_drs_version')) ? $request->cookie('b_drs_version') : $request->version;
            $request->device = ($request->cookie('b_drs_device')) ? $request->cookie('b_drs_device') : $request->device; 
            $request->url = ($request->cookie('b_drs_url')) ? $request->cookie('b_drs_url') : $request->url;
            $isUpdateNull = is_null($request->cookie('b_drs_is_update'));

            if( $isUpdateNull )
            {
                $request->is_update = '';
                Cookie::queue('b_drs_is_update', $request->is_update, 60);
            }
            else
            {
                $request->is_update = ( $request->cookie('b_drs_is_update') ) ? $request->cookie('b_drs_is_update') : $request->is_update . '';
            }
        }
        else
        {
            Cookie::queue('b_drs_version', $request->version, 60);
            Cookie::queue('b_drs_device', $request->device, 60);  
            Cookie::queue('b_drs_url', $request->url, 60);            
            Cookie::queue('b_drs_is_update', $request->is_update . "", 60);
        }

        $version = $request->version;
        $device = $request->device;
        $url = $request->url;    
        $is_update = $request->is_update;
       
        return $this->version->searchVersions($pageLimit, $version, $device, $url, $is_update);
    }

    /**
     * 取得單筆版本
     *
     * @param integer $id
     * @return void
     */
    function getVersion($id = 0)
    {
        return $this->version->getVersion($id);
    }

    /**
     * 新增版本
     *
     * @param Request $request
     * @return void
     */
    function insertVersion(Request $request)
    {
        
        $validateRules = [
            'version' => 'required|max:10',
            'message' => 'max:100',
            'url' => 'required|max:200',
        ];

        $validateMessage = [
            'version.required' => MessagePresenter::getRequired('版本','text'),
            'version.max' => MessagePresenter::getMax('版本', 10),
            'url.required' => MessagePresenter::getRequired('url','text'),
            'url.max' => MessagePresenter::getMax('url', 200),
            'message.max' => MessagePresenter::getMax('訊息', 100),
        ];

        $request->validate($validateRules, $validateMessage);

        return $this->version->insertVersion($request);
    }

    /**
     * 修改版本資料
     *
     * @param Request $request
     * @return void
     */
    function modifyVersion(Request $request)
    {
        $validateRules = [
            'version' => 'required|max:10',
            'message' => 'max:100',
            'url' => 'required|max:200',
        ];

        $validateMessage = [
            'version.required' => MessagePresenter::getValidatorMessage('version.required'),
            'version.max' => MessagePresenter::getValidatorMessage('version.max', 10),
            'url.required' => MessagePresenter::getValidatorMessage('url.required'),
            'url.max' => MessagePresenter::getValidatorMessage('url.max', 200),
            'message.max' => MessagePresenter::getValidatorMessage('tel.max', 100),
        ];

        $request->validate($validateRules, $validateMessage);

        return $this->version->modifyVersion($request);
    }

    /**
     * 刪除版本
     *
     * @param integer $id
     * @return void
     */
    function deleteVersion($id = 0)
    {       
        return $this->version->deleteVersion($id);
    }

    /**
     * 取得最新裝置版本
     * 
     * @param string $device
     * @return void
     */
    function getLatestVersion($device = null)
    {
        return $this->version->getLatestVersion($device);
    }
}