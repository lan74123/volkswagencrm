<?php
namespace App\Services\Backend;

use App\Repository as Repo;
use Illuminate\Http\Request;
use App\Presenter\MessagePresenter;

class CampaignService
{
    //Campaign Repository
    protected $campaign;

    /**
     * 建構子
     *
     * @param Repo\Backend\CampaignRepository $group
     */
    public function __construct(Repo\Backend\CampaignRepository $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * 取得所有活動
     *
     * @return void
     */
    public function getAllCampaigns()
    {
        return $this->campaign->getAllCampaigns();
    }

    /**
     * 取得活動
     *
     * @param integer $id
     * @return void
     */
    public function getCampaign($id = 0)
    {
        return $this->campaign->getCampaign($id);   
    }

    /**
     * 新增活動
     *
     * @param Request $request
     * @return void
     */
    public function insertCampaign(Request $request)
    {
        $validateRules = [
            'name' => 'required|max:50',
            'place' => 'max:100',
            'type' => 'required',
            'image' => 'required|image|mimes:jpeg,png,bmp',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'description' => 'required',
        ];

        $validateMessage = [
            'name.required' => MessagePresenter::getRequired('活動名稱'),
            'name.max' => MessagePresenter::getMax('活動名稱', 50),
            'place.max' => MessagePresenter::getMax('活動地點', 100),
            'type.required' => MessagePresenter::getRequired('活動種類', 'option'),
            'image.required' => MessagePresenter::getRequired('活動圖片', 'file'),
            'image.image' => MessagePresenter::getImage('活動圖片'),
            'image.mimes' => MessagePresenter::getMimes('活動圖片', 'JPG, PNG, BMP'),
            'start_date.required' => MessagePresenter::getRequired('活動起始日期', 'option'),
            'start_date.date' => MessagePresenter::getDate('活動起始日期'),
            'end_date.required' => MessagePresenter::getRequired('活動結束日期', 'option'),
            'end_date.date' => MessagePresenter::getDate('活動結束日期'),
            'description.required' => MessagePresenter::getRequired('活動敘述'),
        ];

        $request->validate($validateRules, $validateMessage);

        $campaignId = $this->campaign->insertCampaign($request);

        return $campaignId;
    }

    /**
     * 修改活動
     *
     * @param Request $request
     * @param integer $campaignId
     * @return void
     */
    public function modifyCampaign(Request $request, $campaignId = 0)
    {
        $validateRules = [
            'name' => 'required|max:50',
            'place' => 'max:100',
            'type' => 'required',
            'image' => 'image|mimes:jpeg,png,bmp',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'description' => 'required',
        ];

        $validateMessage = [
            'name.required' => MessagePresenter::getRequired('活動名稱'),
            'name.max' => MessagePresenter::getMax('活動名稱', 50),
            'place.max' => MessagePresenter::getMax('活動地點', 100),
            'type.required' => MessagePresenter::getRequired('活動種類', 'option'),
            'image.image' => MessagePresenter::getImage('活動圖片'),
            'image.mimes' => MessagePresenter::getMimes('活動圖片', 'JPG, PNG, BMP'),
            'start_date.required' => MessagePresenter::getRequired('活動起始日期', 'option'),
            'start_date.date' => MessagePresenter::getDate('活動起始日期'),
            'end_date.required' => MessagePresenter::getRequired('活動結束日期', 'option'),
            'end_date.date' => MessagePresenter::getDate('活動結束日期'),
            'description.required' => MessagePresenter::getRequired('活動敘述'),
        ];

        $request->validate($validateRules, $validateMessage);

        $isSuccessful = $this->campaign->modifyCampaign($request, $campaignId);

        return $isSuccessful;
    }

    /**
     * 刪除活動
     *
     * @param integer $groupId
     * @return void
     */
    public function deleteCampaign($campaignId = 0)
    {
        $isSuccessful = $this->campaign->deleteCampaign($campaignId);

        return $isSuccessful;
    }
}