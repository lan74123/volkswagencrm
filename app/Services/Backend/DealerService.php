<?php
namespace App\Services\Backend;


use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Repository as Repo;
use App\Presenter\MessagePresenter;
use App;
use Cookie;

class DealerService
{
    //經銷商Service
    protected $dealer;

    /**
     * 建構子
     *
     * @param Repo\Backend\Dealer $dealersitory
     */
    public function __construct(Repo\Backend\DealerRepository $dealer)
    {
        $this->dealer = $dealer;
    }

    /**
     * 搜尋多筆經銷商
     *
     * @param Request $request
     * @param integer $pageLimit
     * @return void
     */
    function searchDealer(Request $request, $pageLimit = 0)
    {
        if ( $_SERVER['REQUEST_METHOD'] == "GET" )
        {          
            $request->area = ($request->cookie('b_drs_area')) ? $request->cookie('b_drs_area') : $request->area;
            $request->name = ($request->cookie('b_drs_name')) ? $request->cookie('b_drs_name') : $request->name; 
            $request->tel = ($request->cookie('b_drs_tel')) ? $request->cookie('b_drs_tel') : $request->tel;
            $isValidNull = is_null($request->cookie('b_drs_valid'));
            
            if( $isValidNull )
            {
                $request->valid = '1';
                Cookie::queue('b_drs_valid', $request->valid, 60);
            }
            else
            {
                $request->valid = ( $request->cookie('b_drs_valid') ) ? $request->cookie('b_drs_valid') : $request->valid.'';
            }

        }
        else
        {
            Cookie::queue('b_drs_area', $request->area, 60);
            Cookie::queue('b_drs_name', $request->name, 60);  
            Cookie::queue('b_drs_tel', $request->tel, 60);            
            Cookie::queue('b_drs_valid', $request->valid."", 60);
        }

        $area = $request->area;
        $name = $request->name;
        $tel = $request->tel;    
        $valid = $request->valid;
        
       
        return $this->dealer->searchDealers($pageLimit, $area, $name, $tel, $valid);
    }

    /**
     * 取得單筆經銷商
     *
     * @param integer $id
     * @return void
     */
    function getDealer($id = 0)
    {
        return $this->dealer->getDealer($id);
    }

    /**
     * 新增經銷商
     *
     * @param Request $request
     * @return void
     */
    function insertDealer(Request $request)
    {
        
        $validateRules = [
            'area' => 'required|max:10',
            'name' => 'unique:dealers|required|max:50',
            'code' => 'max:10',
            'tel' => 'max:20',
            'fax' => 'max:20',
            'address' => 'max:100',
            'email' => [
                'max:200', function($attribute, $value, $fail) 
                {
                    $emailArray = explode(',', $value);                   
                    foreach($emailArray as $email)                   
                    {
                        if(!empty($email))
                        {
                            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
                            {
                                return $fail(' email: ' . $email . ' is invalid.');
                            }
                        }
                    }
                },
            ], 
            'emailcc' => [
                'max:200', function($attribute, $value, $fail) 
                {
                    $emailccArray = explode(',', $value);                   
                    foreach($emailccArray as $email)                   
                    {
                        if(!empty($email))
                        {
                            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
                            {
                                return $fail(' emailcc: ' . $email . ' is invalid.');
                            }
                        }
                    }
                },
            ],         
        ];

        $validateMessage = [            
            'area.required' => MessagePresenter::getRequired('經銷商區域','option'),
            'area.max' => MessagePresenter::getMax('經銷商區域', 10),
            'name.required' => MessagePresenter::getRequired('經銷商名稱','text'),
            'name.unique' => MessagePresenter::getUnique('經銷商名稱'),
            'name.max' => MessagePresenter::getMax('經銷商名稱', 50),
            'tel.max' => MessagePresenter::getMax('經銷商連絡方式', 20),
            'fax.max' => MessagePresenter::getMax('經銷商傳真方式', 20),
            'address.max' => MessagePresenter::getMax('經銷商地址', 100),          
            'email.max' => MessagePresenter::getMax('經銷商EMAIL', 200),
            'emailcc.max' => MessagePresenter::getMax('經銷商EMAIL CC', 200),
        ];

        $request->validate($validateRules, $validateMessage);

        return $this->dealer->insertDealer($request);
    }

    /**
     * 修改經銷商資料
     *
     * @param Request $request
     * @return void
     */
    function modifyDealer(Request $request)
    {
        $validateRules = [
            'area' => 'required|max:10',
            'name' => 'required|max:50',
            'name' =>  Rule::unique('dealers')->ignore($request->id, 'id'),
            'code' => 'max:10',
            'tel' => 'max:20',
            'fax' => 'max:20',
            'address' => 'max:100',
            'email' => [
                'max:200', function($attribute, $value, $fail) 
                {
                    $emailArray = explode(',', $value);                   
                    foreach($emailArray as $email)                   
                    {
                        if(!empty($email))
                        {
                            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
                            {
                                return $fail(' email: ' . $email . ' is invalid.');
                            }
                        }
                    }
                },
            ], 
            'emailcc' => [
                'max:200', function($attribute, $value, $fail) 
                {
                    $emailccArray = explode(',', $value);                   
                    foreach($emailccArray as $email)                   
                    {
                        if(!empty($email))
                        {
                            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
                            {
                                return $fail(' emailcc: ' . $email . ' is invalid.');
                            }
                        }
                    }
                },
            ],           
        ];

        $validateMessage = [
            'area.required' => MessagePresenter::getRequired('經銷商區域','option'),
            'area.max' => MessagePresenter::getMax('經銷商區域', 10),
            'name.required' => MessagePresenter::getRequired('經銷商名稱','text'),
            'name.unique' => MessagePresenter::getUnique('經銷商名稱'),
            'name.max' => MessagePresenter::getMax('經銷商名稱', 50),
            'tel.max' => MessagePresenter::getMax('經銷商連絡方式', 20),
            'fax.max' => MessagePresenter::getMax('經銷商傳真方式', 20),
            'address.max' => MessagePresenter::getMax('經銷商地址', 100),          
            'email.max' => MessagePresenter::getMax('經銷商EMAIL', 200),
            'emailcc.max' => MessagePresenter::getMax('經銷商EMAIL CC', 200),  
        ];

        $request->validate($validateRules, $validateMessage);   
             
        return $this->dealer->modifyDealer($request);
    }

    /**
     * 刪除經銷商
     *
     * @param integer $id
     * @return void
     */
    function deleteDealer($id = 0)
    {       
        return $this->dealer->deleteDealer($id);
    }
}