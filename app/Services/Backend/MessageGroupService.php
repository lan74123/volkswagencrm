<?php
namespace App\Services\Backend;


use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Repository as Repo;
use App\Presenter\MessagePresenter;
use App;
use Cookie;

class MessageGroupService
{
    //訊息群組Service
    protected $messageGroup;

    /**
     * 建構子
     *
     * @param Repo\Backend\MessageGroup $messageGroupsitory
     */
    public function __construct(Repo\Backend\MessageGroupRepository $messageGroup)
    {
        $this->messageGroup = $messageGroup;
    }

    /**
     * 搜尋多筆訊息群組
     *
     * @param Request $request
     * @param integer $pageLimit
     * @return void
     */
    function searchMessageGroup(Request $request, $pageLimit = 0)
    {
        if ( $_SERVER['REQUEST_METHOD'] == "GET" )
        {
            $request->name = ($request->cookie('b_msg_name')) ? $request->cookie('b_msg_name') : $request->name;         
            $isValidNull = is_null($request->cookie('b_msg_valid'));
            $request->date_start = ($request->cookie('b_msg_date_start')) ? $request->cookie('b_msg_date_start') : $request->date_start;
            $request->date_end= ($request->cookie('b_msg_date_end')) ? $request->cookie('b_msg_date_end') : $request->date_end;
        }
        else
        {
            Cookie::queue('b_msg_name', $request->name, 60);     
            Cookie::queue('b_msg_date_start', $request->date_start, 60);
            Cookie::queue('b_msg_date_end', $request->date_end, 60);           
        }

        $name = $request->name; 
        $date_start = $request->date_start;
        $date_end = $request->date_end;
        $valid = $request->valid;
       
        return $this->messageGroup->searchMessageGroups($pageLimit, $name, $date_start, $date_end, $valid);
    }

    /**
     * 取得單筆訊息群組
     *
     * @param integer $id
     * @return void
     */
    function getMessageGroup($id = 0)
    {
        return $this->messageGroup->getMessageGroup($id);
    }

    /**
     * 新增訊息群組
     *
     * @param Request $request
     * @return void
     */
    function insertMessageGroup(Request $request)
    {
        
        $validateRules = [            
            'name' => 'unique:message_groups|required|max:50',
            'desc' => 'max:200',      
        ];
        
        $validateMessage = [
            'name.required' => MessagePresenter::getRequired('溝通群組名稱'),
            'name.max' => MessagePresenter::getMax('溝通群組名稱', 50),
            'name.unique' => MessagePresenter::getUnique('溝通群組名稱'),
            'desc.max' => MessagePresenter::getMax('溝通群組描述', 200),
        ];

        $request->validate($validateRules, $validateMessage);

        return $this->messageGroup->insertMessageGroup($request);
    }

    /**
     * 新增訊息群組名單
     *
     * @param Request $request
     * @return void
     */
    function insertMessageGroupMember($message_groups_id = 0, $member_id = 0)
    {
        return $this->messageGroup->insertMessageGroupMember($message_groups_id,$member_id);
    }

    /**
     * 修改訊息群組資料
     *
     * @param Request $request
     * @return void
     */
    function modifyMessageGroup(Request $request)
    {
        $validateRules = [            
            'name' => 'required|max:50',  
            'name' =>  Rule::unique('message_groups')->ignore($request->id, 'id'),            
            'desc' => 'max:200',            
        ];

        $validateMessage = [
            'name.required' => MessagePresenter::getRequired('溝通群組名稱'),
            'name.max' => MessagePresenter::getMax('溝通群組名稱', 50), 
            'name.unique' => MessagePresenter::getUnique('溝通群組名稱'),  
            'desc.max' => MessagePresenter::getMax('溝通群組描述', 200),
        ];


        $request->validate($validateRules, $validateMessage);   
             
        return $this->messageGroup->modifyMessageGroup($request);
    }

    /**
     * 取得訊息群組名單
     *
     * @param Request $request
     * @return void
     */
    function getMessageGroupMember($pageLimit = 0, $message_groups_id = 0)
    {
        return $this->messageGroup->getMessageGroupMember($pageLimit, $message_groups_id);
    }

    /**
     * 刪除訊息群組
     *
     * @param integer $id
     * @return void
     */
    function deleteMessageGroup($id = 0)
    {       
        return $this->messageGroup->deleteMessageGroup($id);
    }

    /**
     * 刪除訊息群組名單
     *
     * @param integer $group_id
     * @return void
     */
    function deleteMessageGroupMemberByGroup($group_id = 0)
    {       
        return $this->messageGroup->deleteMessageGroupMemberByGroup($group_id);
    }


     /**
     * 刪除訊息群組名單
     *
     * @param integer $id
     * @return void
     */
    function deleteMessageGroupMember($id = 0)
    {       
        return $this->messageGroup->deleteMessageGroupMember($id);
    }
}