<?php
namespace App\Services\Common;


use Illuminate\Http\Request;
use App\Repository as Repo;
use App\Services as Service;
use App\Presenter\MessagePresenter;
use App;
use Cookie;

class MaintainsService
{
    //repo
    protected $maintains;

    /**
     * 建構子
     *
     * @param Repo\Common\MaintainsRepository $maintains
     */
    public function __construct(Repo\Common\MaintainsRepository $maintains)
    {
        $this->maintains = $maintains;      
    }

    /**
     * 複製保養資料
     *
     * @param string $origin_id
     * @param string $vin
     * @param string $license_plate_number
     * @return void
     */
    function copyMaintainsData($origin_id = null, $vin = null, $license_plate_number = null)
    {        
        return $this->maintains->copyMaintainsData($origin_id, $vin, $license_plate_number);
    }

    /**
     * 刪除保養資料
     *
     * @param string $origin_id
     * @param string $vin
     * @param string $license_plate_number
     * @return void
     */
    function deleteMaintainsData($origin_id = null, $vin = null, $license_plate_number = null)
    {        
        return $this->maintains->deleteMaintainsData($origin_id, $vin, $license_plate_number);
    }    
    
}