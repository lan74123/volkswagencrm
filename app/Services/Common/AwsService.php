<?php

namespace App\Services;

use Aws\CloudFront;
use Storage;

class AwsService
{
    public function __construct()
    {
        
        $this->cdn_client = new CloudFront\CloudFrontClient([
            'region'  => env('AWS_REGION'),
            'version' => 'latest',
            'credentials' => [
                'key'    => env('AWS_KEY'),
                'secret' => env('AWS_SECRET'),
            ],
        ]);
        
    }

    public function saveToS3($img ,$path)
    {
        $image = $img ;
        $image_thumb = $image->stream();
        Storage::disk('s3')->put($path, $image_thumb->__toString());
        //$this->createInvalidation($path);
    }

    public function saveFileToS3($path,$file,$fileName)
    {
        Storage::disk('s3')->putFileAs($path, $file,$fileName);
    }

    public function deleteToS3($path)
    {
        Storage::disk('s3')->delete($path);
    }


    public function createInvalidation($path)
    {
        $result = $this->cdn_client->createInvalidation([
            'DistributionId' => env('CDN_ID'),
            'InvalidationBatch' => [
                'CallerReference' => time().$this->getGUID(),
                'Paths' => [
                    'Items' => ["/".$path],
                    'Quantity' => 1,
                ],
            ],
        ]);

        return $result;
    }

    function getGUID(){
        if (function_exists('com_create_guid')){
            return com_create_guid();
        }else{
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtolower(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12)
            ;// "}"
            return $uuid;
        }
    }

}