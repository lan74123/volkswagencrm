<?php
namespace App\Services\Common;


use Illuminate\Http\Request;
use App\Repository as Repo;
use App\Services as Service;
use App\Presenter\MessagePresenter;
use App;
use Cookie;

class DevicePushService
{
    //repo
    protected $devicepush;

    /**
     * 建構子
     *
     * @param Repo\Common\DevicePushRepository $devicepush
     */
    public function __construct(Repo\Common\DevicePushRepository $devicepush)
    {
        $this->devicepush = $devicepush;      
    }

    /**
     *  API 取得 裝置推播
     */
    function getDevicePush($device_id = null)
    { 
         

        return $this->devicepush->getDevicePush($device_id);
    }

    /**
     *  API 新增裝置推播
     */
    function insertDevicePush($device_id = null, $push_token = null, $device = null)
    {   
        return  $this->devicepush->insertDevicePush($device_id, $push_token, $device);       
    }

    /**
     *  API 更新裝置推播
     */
    function modifyDevicePush($id = 0, $member_id = null, $push_token = null)
    {    
        return $this->devicepush->modifyDevicePush($id, $member_id, $push_token);
    }
    
}