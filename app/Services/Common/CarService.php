<?php
namespace App\Services\Common;


use Illuminate\Http\Request;
use App\Repository as Repo;
use App\Services as Service;
use App\Presenter\MessagePresenter;
use App;
use Cookie;
use Validator;
class CarService
{
    //車輛Service
    protected $car, $member, $dealer, $maintainsService;

    /**
     * 建構子
     *
     * @param Repo\Common\Car $carsitory
     */
    public function __construct(Repo\Common\CarRepository $car, Repo\Common\MemberRepository $member,
    Repo\Backend\DealerRepository $dealer, Service\Common\MaintainsService $maintainsService)
    {
        $this->car = $car;
        $this->member = $member;
        $this->dealer = $dealer;
        $this->maintainsService = $maintainsService;
    }

    /**
     * 搜尋會員身上車輛
     *
     * @param Request $request
     * @param integer $pageLimit
     * @return void
     */
    function searchMemberCar($pageLimit = 0, $member_id = 0 ,$license_plate_number = null, $sort = 'desc')
    {        
        return $this->car->searchMemberCars($pageLimit, $member_id, null, $sort);
    }    

     /**
     * 後台搜尋車輛比對資料庫
     *
     * @param Request $request
     * @param integer $pageLimit
     * @return void
     */
    function searchCarOriginForBackend($pageLimit = 0, Request $request)
    {        
        $name = $request->name;
        $vin = $request->vin;
        $license_plate_number = $request->license_plate_number; 

        return $this->car->searchCarOriginForBackend($pageLimit, $name, $vin, $license_plate_number);
    } 

    /**
     * 搜尋車輛比對資料庫
     *
     * @param Request $request
     * @param integer $pageLimit
     * @return void
     */
    function searchCarOrigin(Request $request)
    {        
        $name = $request->name;
        $vin = $request->VIN;
        $license_plate_number = $request->LicensePlateNumber; 

        return $this->car->searchCarOrigin($name, $vin, $license_plate_number);
    } 
    
    /**
     * 搜尋車輛
     *
     * @param Request $request
     * @param integer $pageLimit
     * @return void
     */
    function searchCar($member_id = 0, Request $request)
    {
        $vin = $request->VIN;
        $license_plate_number = $request->LicensePlateNumber; 

        return $this->car->searchCar($member_id, $vin, $license_plate_number);
    }

    /**
     * 新增車輛
     *
     * @param int $member_id
     * @param Request $request
     * name
     * vin
     * license_plate_number 
     * @return void
     */
    function insertCar($member_id = 0, Request $request)
    { 
        $pageLimit = 0;
        //檢查會員車輛數量
        $member_car = $this->car->searchMemberCars($pageLimit, $member_id, null, 'desc');
        
        if(count($member_car) >= 3)
        {            
            return '超過車輛上限:3';            
        } 

        $car_check = null;
        $vin = $request->VIN;
        $license_plate_number = $request->LicensePlateNumber;

        $messages = [
            'VIN.min' => 'VIN太短',           
            'VIN.max' => 'VIN過長',
            'LicensePlateNumber.required' => '車牌未填'
        ];    

        $validator = Validator::make(
            [                
                'VIN' => $vin,
                'LicensePlateNumber' => $license_plate_number
            ],
            [               
                'VIN' => 'max:20|min:5',
                'LicensePlateNumber' => 'required'
            ],
            $messages);

        if ($validator->fails())
        {
            $msg='';

            foreach($validator->messages()->all() as $key => $value)
            {

                $msg .=  $value . '  ';
            }           
            return $msg;
        }
        
        //檢查會員車輛是否已加入
        foreach($member_car as $car)
        {          
            if(str_contains($car->vin, $vin) && $car->license_plate_number == $license_plate_number && $member_id == $car->member_id)
            {
                $car_check = $car;
            }
        } 
       
        if(!is_null($car_check))
        {  
            return '車輛已存在';               
        }
    
        //檢查比對資料庫
        $car_origin = $this->searchCarOrigin($request);
     
        if(is_null($car_origin))
        {
            return '車輛資料不存在';
        }

        //取得經銷商ID
        $dealer = $this->dealer->searchDealers(1, null, $car_origin->dealer, null, null);

        $dealer_id = 0;

        foreach($dealer as $d)
        {
            $dealer_id = $d->id;
        }
       
        $car_origin['dealer_id'] = $dealer_id;

        //取得車型ID
        $car_origin->car_type;
       
        $member_name = $request->Name;
        //會員新增資料
        $result = $this->car->memberInsertCar($member_name, $member_id, $car_origin);  
        
        //複製保養資料
        $this->maintainsService->copyMaintainsData($car_origin->origin_id, $vin, $license_plate_number);
        
        if($result == 'Y')
        {
            return '車輛加入成功';
        }
        else
        {
            return '車輛加入失敗，請稍後再試。';
        }

    }   

    /**
     * 刪除車輛
     *
     * @param int $member_id
     * @param Request $request
     * name
     * vin
     * license_plate_number 
     * @return void
     */
    function deleteCar($member_id = 0, Request $request)
    {       
        $pageLimit = 0;

        //檢查會員車輛數量
        $member_car = $this->car->searchMemberCars($pageLimit, $member_id, null, 'desc');
        
        if(count($member_car) <= 1)
        {      
            return '車輛最少一輛';            
        } 

        $car_check = null;
        $vin = $request->VIN;
        $license_plate_number = $request->LicensePlateNumber; 
        //檢查會員車輛是否已加入
        foreach($member_car as $car)
        {      
            if(str_contains($car->vin, $vin) && $car->license_plate_number == $license_plate_number && $member_id == $car->member_id)
            {
                $car_check = $car;
            }
        }  
     
        if(is_null($car_check))
        {           
            return '車輛不存在';
        }     

        //檢查比對資料庫
        $car_origin = $this->searchCarOrigin($request);

        //檢查比對資料庫
        if(is_null($car_origin))
        {
            return '車輛資料不存在';
        }

        $car_id = $car_check->id;    
         
        //會員刪除資料
        $result = $this->car->memberDeleteCar($car_check->member->name, $car_id, $car_origin);
        
        //刪除保養資料
        $this->maintainsService->deleteMaintainsData($car_origin->origin_id, $vin, $license_plate_number);

        if($result == 'Y')
        {
            return '車輛移除成功';
        }
        else
        {
            return '車輛移除失敗，請稍後再試。'.$result;
        }       
    }   


    /**
     * getCarMaintains
     *
     * @param integer $member_id
     * @param Request $request
     * @return void
     */
    function getCarMaintains($origin_id = 0, $vin = 0, $license_plate_number = 0)
    {
        return $this->car->getCarMaintains($origin_id ,$vin ,$license_plate_number);
    }

}