<?php
namespace App\Services\Common;

use Mail;
use App;


class EmailService
{
   public function sendMail($subject = '', $to = '', $cc = '', $bcc = '', $content = '')
   {

        $data = [
            'title' => $subject, 
            'content' => $content
        ];

        $mail = Mail::to($to);

        if($cc != '')
        {
            $mail->cc($cc);
        }
        
        if($bcc != '')
        {
            $mail->bcc($bcc);
        }
      
        $mail->send(new \App\Mail\TemplateMail($subject,$content));

        // Mail::raw('Text to e-mail', function ($message) use ($from,$fromName,$subject,$to,$cc,$bcc,$content)
        // {   
            
        //     // $message->from('us@example.com', 'Laravel');

        //     // $message->to('foo@example.com')->cc('bar@example.com');
     
        //     $message->from($from);
        //     $message->subject($subject); 
        //     $message->to($to);
        //     $message->cc($cc);
        //     $message->bcc($bcc);
        //     $message->setBody($content);                          
        // });
   }
}