<?php
namespace App\Services\Common;


use Illuminate\Http\Request;
use App\Repository as Repo;
use App\Services as Service;
use App\Presenter\MessagePresenter;
use App;
use Cookie;
use Validator;

class MemberService
{
    //repo
    protected $member, $car, $carService;

    /**
     * 建構子
     *
     * @param Repo\Common\Member $membersitory
     */
    public function __construct(Repo\Common\MemberRepository $member, Repo\Common\CarRepository $car,
    Service\Common\CarService $carService)
    {
        $this->member = $member;
        $this->car = $car;
        $this->carService = $carService;
    }

    /**
     * 搜尋多筆會員
     *
     * @param Request $request
     * @param integer $pageLimit
     * @return void
     */
    function searchMember(Request $request, $pageLimit = 0)
    {
        if ( $_SERVER['REQUEST_METHOD'] == "GET" )
        {          
            $request->name = ($request->cookie('b_mbs_name')) ? $request->cookie('b_mbs_name') : $request->name; 
            $request->mobile = ($request->cookie('b_mbs_mobile')) ? $request->cookie('b_mbs_mobile') : $request->mobile; 
            $request->date_start = ($request->cookie('b_mbs_date_start')) ? $request->cookie('b_mbs_date_start') : $request->date_start;
            $request->date_end= ($request->cookie('b_mbs_date_end')) ? $request->cookie('b_mbs_date_end') : $request->date_end;

            $isValidNull = is_null($request->cookie('b_mbs_valid'));

            if( $isValidNull )
            {
                $request->valid = '';
                Cookie::queue('b_mbs_valid', $request->valid, 60);
            }
            else
            {
                $request->valid = ( $request->cookie('b_mbs_valid') ) ? $request->cookie('b_mbs_valid') : $request->valid.'';
            }
        }
        else
        {
            Cookie::queue('b_mbs_name', $request->name, 60);  
            Cookie::queue('b_mbs_mobile', $request->mobile, 60);
            Cookie::queue('b_mbs_date_start', $request->date_start, 60);
            Cookie::queue('b_mbs_date_end', $request->date_end, 60);
            Cookie::queue('b_mbs_valid', $request->valid."", 60);
        }

        $name = $request->name;
        $mobile = $request->mobile;
        $date_start = $request->date_start;
        $date_end = $request->date_end;
        $valid = $request->valid;
       
        return $this->member->searchMembers($pageLimit, $name, $mobile, $date_start, $date_end, $valid);
    }

    /**
     * 搜尋多筆會員
     *
     * @param Request $request
     * @param integer $pageLimit
     * @return void
     */
    function searchMemberByGroup(Request $request, $pageLimit = 0)
    {       

        $name = $request->name;
        $mobile = $request->mobile;
        $last_maintenance = $request->last_maintenance;
        $car_model = $request->car_model;
        $mileage = $request->mileage;
        $date_start = $request->date_start;
        $date_end = $request->date_end;       
       
        return $this->member->searchMembersForGroup($pageLimit, $name, $mobile, $last_maintenance, $car_model, $mileage, $date_start, $date_end);
    }

    /**
     * 取得單筆會員
     *
     * @param integer $id
     * @return void
     */
    function getMember($id = 0)
    {
        return $this->member->getMember($id);
    }

    /**
     * 新增會員
     *
     * @param Request $request
     * @return void
     */
    function insertMember(Request $request)
    {
        $validateRules = [
            'name' => 'required|max:100',
            'email' => 'required|email|max:200|unique:members',
//            'password' => 'required',
            'mobile' => 'max:20',
            'address' => 'max:100',
        ];

        $validateMessage = [           
            'name.required' => MessagePresenter::getRequired('姓名','text'),
            'name.max' => MessagePresenter::getMax('姓名', 50),
            'email.required' => MessagePresenter::getRequired('帳號','text'),
            'email.email' => MessagePresenter::getMimes('帳號','email'),
            'email.max' => MessagePresenter::getMax('經銷商EMAIL', 200),             
 //           'password.required' => MessagePresenter::getRequired('密碼'),
            'mobile.max' => MessagePresenter::getMax('連絡電話', 20),
            'address.max' => MessagePresenter::getMax('連絡地址', 100),   
        ];

        $request->validate($validateRules, $validateMessage);   

        return $this->member->insertMember($request);
    }

    /**
     * 修改會員資料
     *
     * @param Request $request
     * @return void
     */
    function modifyMember(Request $request)
    {
        $validateRules = [
            'name' => 'required|max:100',
            'email' => 'required|email|max:200',
            'mobile' => 'max:20',
            'address' => 'max:100',           
        ];

        $validateMessage = [

            'name.required' => MessagePresenter::getRequired('姓名','text'),
            'name.max' => MessagePresenter::getMax('姓名', 50),
            'email.required' => MessagePresenter::getRequired('帳號','text'),
            'email.email' => MessagePresenter::getMimes('帳號','email'),
            'email.max' => MessagePresenter::getMax('經銷商EMAIL', 200),             
            'mobile.max' => MessagePresenter::getMax('連絡電話', 20),
            'address.max' => MessagePresenter::getMax('連絡地址', 100), 
                 
        ];

        $request->validate($validateRules, $validateMessage);   
             
        return $this->member->modifyMember($request);
    }

    /**
     * 刪除會員
     *
     * @param integer $id
     * @return void
     */
    function deleteMember($id = 0)
    {       
        return $this->member->deleteMember($id);
    }


    /**
     *  API 會員註冊
     */
    function registerMember(Request $request)
    {   

        $name = $request->Name;
        $vin = $request->VIN;
        $license_plate_number = $request->LicensePlateNumber;
        $mobile = $request->Mobile;
        $device_id = $request->DeviceID;
        $fbid = $request->FBID;
        $agree_data_use = $request->AgreeDataUse;

        $data = [];
        $data['member'] = null;
        
        if(is_null($license_plate_number))
        {
            $data['msg'] = '請輸入車牌';           
            return $data;
        }

        if(is_null($vin))
        {
            $data['msg'] = '請輸入VIN';           
            return $data;
        }

        if(is_null($mobile))
        {
            $data['msg'] = '請輸入手機';           
            return $data;
        }

        //尋找註冊會員
        $member = $this->member->searchMemberByName($name);

        //尋找比對會員資料庫
        $car_origin = $this->car->searchCarOrigin($name, $vin, $license_plate_number);         

        if(is_null($car_origin))
        {
            $data['msg'] = '車輛資料不存在';           
            return $data;
        }       
 
        //未註冊會員
        if(is_null($member))
        {
           
            $new_member = $this->member->registerMember($car_origin->origin_id, $name, $mobile, $device_id, $fbid, $agree_data_use);         
            $member = $new_member;
           
            if(is_null($new_member))
            {
                $member->msg = '車主資料新增失敗';
                return $member;
            }

            $request['vin'] = $request->VIN;
            $request['license_plate_number'] = $request->LicensePlateNumber;
            $request['name'] = $request->Name;

            $data['msg'] = $this->carService->insertCar($new_member->id, $request);
        }
        else
        {
            //update fbid
            $this->updateMember($member->id, null, null, null, $fbid, 'fb');

            $request['vin'] = $request->VIN;
            $request['license_plate_number'] = $request->LicensePlateNumber;
            $request['name'] = $request->Name;

            $data['msg'] = $this->carService->insertCar($member->id, $request);          
        }
 
        $data['member'] = $member;

        return $data;
    }

    /**
     *  API 會員登入
     */
    function memberLogin($name = '')
    {  
        $data = [];
       
        $data['member'] = null;

        //尋找註冊會員
        $member = $this->member->searchMemberByName($name);
       
        //未註冊會員
        if(is_null($member))
        {
           $data['msg'] = '無法自動登入請重新登入';
        }
        else
        {
            $data['msg'] = '';
        }
        
        $data['member'] = $member;

        return $data;
    }

    /**
     *  API 更新會員資料
     */
    function updateMember($id = 0, $email = null, $mobile = null, $address = null, $fbid = null , $type = 'fb')
    {     
        $data = [];
        $data['member'] = null;

        if($type != 'fb')
        {
            $email = (is_null($email))? '' : $email;
            $mobile = (is_null($mobile))? '' : $mobile;
            $address = (is_null($address))? '' : $address;
        
            $messages = [
                'mobile.regex' => '電話格式錯誤',
                'email.email' => 'email格式錯誤',
                'address.max' => '地址過長',
            ];    

            $validator = Validator::make(
                [                
                    'mobile' => $mobile,
                    'address' => $address,
                    'email' => $email
                ],
                [               
                    'mobile' => 'sometimes|regex:/^09[\d]{8}$/',
                    'address' => 'sometimes|max:50',
                    'email' => 'sometimes|email'
                ]
            ,$messages);

            if ($validator->fails())
            {
                $msg='';

                foreach($validator->messages()->all() as $key => $value)
                {

                    $msg .=  $value . '  ';
                }
                $data['msg'] = $msg;
                return $data;
            }

            $member = $this->member->updateMember($id, $email, $mobile, $address, $fbid, $type);
        }
        else
        {
            $member = $this->member->updateMember($id, null, null, null, $fbid, $type);
        }
       
        
        
        $data['member'] = $member;

        return $data;
    }
    
}