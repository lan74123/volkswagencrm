<?php
namespace App\Services\Common;

use Aws\CloudFront;
use Storage;;
use File;


class FileService
{
    public function saveFile($folder_name = null, $file = null, $fileName = null)
    {
        //$this->saveFileToS3($folder_name,$file,$fileName);
        if (env('APP_ENV') === 'production') 
        {
            $this->saveToS3($file,$folder_name.'/'.$fileName);
        }
        else 
        {
            if(!File::exists($folder_name)) 
            {
                //path does not exist
                Storage::disk('public')->makeDirectory($folder_name);
            }

            Storage::disk('public')->put($folder_name . '/' . $fileName, $file);
        }
    }

    public function deleteFile($path = null)
    {
        Storage::disk('public')->delete($path);
    }


    //aws

    public function __construct()
    {
        
        $this->cdn_client = new CloudFront\CloudFrontClient([
            'region'  => env('AWS_REGION'),
            'version' => 'latest',
            'credentials' => [
                'key'    => env('AWS_KEY'),
                'secret' => env('AWS_SECRET'),
            ],
        ]);
        
    }

    public function saveToS3($img ,$path)
    {
        $image = $img ;
        //$image_thumb = $image->stream();
        Storage::disk('s3')->put($path, $image->__toString());
        //$this->createInvalidation($path);
    }

    public function saveFileToS3($path,$file,$fileName)
    {
        Storage::disk('s3')->putFileAs($path, $file,$fileName);
    }

    public function deleteToS3($path)
    {
        Storage::disk('s3')->delete($path);
    }


    public function createInvalidation($path)
    {
        $result = $this->cdn_client->createInvalidation([
            'DistributionId' => env('CDN_ID'),
            'InvalidationBatch' => [
                'CallerReference' => time().$this->getGUID(),
                'Paths' => [
                    'Items' => ["/".$path],
                    'Quantity' => 1,
                ],
            ],
        ]);

        return $result;
    }

    function getGUID(){
        if (function_exists('com_create_guid')){
            return com_create_guid();
        }else{
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtolower(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12)
            ;// "}"
            return $uuid;
        }
    }
}