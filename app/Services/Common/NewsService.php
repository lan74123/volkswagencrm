<?php
namespace App\Services\Common;


use Illuminate\Http\Request;
use App\Repository as Repo;
use App\Presenter\MessagePresenter;
use App;
use Cookie;
use DateTime;

class NewsService
{
    //News Service
    protected $news;

    /**
     * 建構子
     *
     * @param Repo\Common\News $newssitory
     */
    public function __construct(Repo\Common\NewsRepository $news)
    {
        $this->news = $news;
    }

    /**
     * 搜尋News
     *
     * @param Request $request
     * @param integer $pageLimit
     * @return void
     */
    function searchNews(Request $request, $pageLimit = 0)
    {
        $title = $request->title; 
        $date_start = $request->date_start;
        $date_end = $request->date_end;
        $is_top = $request->is_top;
        $valid = $request->valid;
       
        return $this->news->searchNews($pageLimit, $title, $date_start, $date_end, $is_top, $valid);
    }

    /**
     * 取得單筆News
     *
     * @param integer $id
     * @return void
     */
    function getNews($id = 0)
    {
        return $this->news->getNews($id);
    }

    /**
     * 新增News
     *
     * @param Request $request
     * @return void
     */
    function insertNews(Request $request)
    {
        $validateRules = [
            'type' => 'required',
            'title' => 'required|max:50',
            'short_desc' => 'required|max:200',
            'content_desc' => 'required',
            'info' => 'max:500',
            'img_path' => 'required|image|mimes:jpeg,png,bmp',
            'publish_date' => 'required',
            'url' => 'max:200',
            'valid' => 'required',
            'is_top' => 'required',
        ];

        $validateMessage = [
            'type.required' => MessagePresenter::getRequired('type.required'),
            'title.required' => MessagePresenter::getRequired('title.required'),
            'title.max' => MessagePresenter::getMax('title.max', 50),
            'short_desc.required' => MessagePresenter::getRequired('short_desc.required'),
            'short_desc.max' => MessagePresenter::getMax('short_desc.max', 200),
            'content_desc.required' => MessagePresenter::getRequired('content_desc.required'),
            'info.max' => MessagePresenter::getMax('info.max', 500),
            'img_path.required' => MessagePresenter::getRequired('img_path.required'),
            'img_path.image' => MessagePresenter::getImage('News Pic'),
            'img_path.mimes' => MessagePresenter::getMimes('News Pic', 'JPG, PNG, BMP'),
            'publish_date.required' => MessagePresenter::getRequired('publish_date.required'),
            'url.max' => MessagePresenter::getMax('url.max', 200),
            'valid.required' => MessagePresenter::getRequired('valid.required'),
            'is_top.required' => MessagePresenter::getRequired('is_top.required'),
        ];

        $request->validate($validateRules, $validateMessage);   

        return $this->news->insertNews($request);
    }

    /**
     * 修改News資料
     *
     * @param Request $request
     * @return void
     */
    function modifyNews(Request $request)
    {
        $validateRules = [
            'title' => 'required|max:50',
            'short_desc' => 'required|max:200',
            'content_desc' => 'required',
            'info' => 'max:500',
            'img_path' => 'image|mimes:jpeg,png,bmp',
            'publish_date' => 'required',
            'url' => 'max:200',
            'valid' => 'required',
            'is_top' => 'required',         
        ];

        $validateMessage = [
            'title.required' => MessagePresenter::getRequired('title.required'),
            'title.max' => MessagePresenter::getMax('title.max', 50),
            'short_desc.required' => MessagePresenter::getRequired('short_desc.required'),
            'short_desc.max' => MessagePresenter::getMax('short_desc.max', 200),
            'content_desc.required' => MessagePresenter::getRequired('content_desc.required'),
            'info.max' => MessagePresenter::getMax('info.max', 500),
            'img_path.image' => MessagePresenter::getImage('News Pic'),
            'img_path.mimes' => MessagePresenter::getMimes('News Pic', 'JPG, PNG, BMP'),
            'publish_date.required' => MessagePresenter::getRequired('publish_date.required'),
            'url.max' => MessagePresenter::getMax('url.max', 200),
            'valid.required' => MessagePresenter::getRequired('valid.required'),
            'is_top.required' => MessagePresenter::getRequired('is_top.required'),         
        ];

        $request->validate($validateRules, $validateMessage);   
             
        return $this->news->modifyNews($request);
    }

    /**
     * 刪除News
     *
     * @param integer $id
     * @return void
     */
    function deleteNews($id = 0)
    {       
        return $this->news->deleteNews($id);
    }

    /**
     * 取得News
     *
     * @param Request $request
     * @param integer $pageLimit
     * @return void
     */
    function searchPublicNews($pageLimit = 0, $type = null, $NewsID = null)
    {       
        $news_all =$this->news->searchPublicNews($pageLimit, $type, $NewsID);

        if( $pageLimit == 0 || $pageLimit == 2)
        {
            foreach($news_all as $news)
            {
                if(!is_null($news))
                {
                    $news->img_path = asset('storage/news/' . $news->img_path);
                    $news->short_desc =  preg_replace('/[\r]/', '', $news->short_desc);
                    $news->content_desc = preg_replace('/[\r]/', '', $news->content_desc);
                    $news->info = preg_replace('/[\r]/', '', $news->info);
        
                    // $date = DateTime::createFromFormat("Y/m/d", $news->publish_date);
                    
                    // $news['month'] = $date->format("m");
                    // $news['day'] = $date->format("d");
                }
                
            }
        }
        else
        {
            if(!is_null($news_all))
            {
                $news_all->img_path = asset('storage/news/' . $news_all->img_path);
                $news_all->short_desc =  preg_replace('/[\r]/', '', $news_all->short_desc);
                $news_all->content_desc = preg_replace('/[\r]/', '', $news_all->content_desc);
                $news_all->info = preg_replace('/[\r]/', '', $news_all->info);
            }
        }
      
        return $news_all;
    }
}