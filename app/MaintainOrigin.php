<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaintainOrigin extends Model
{
    protected $table = 'maintains_origins';
    public $timestamps = false;
}
