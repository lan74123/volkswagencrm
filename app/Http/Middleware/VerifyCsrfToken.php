<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        'api/login',
        'api/APP_Version',
        'api/APP_Sign_Up',
        'api/APP_VWID_Login',
        'api/APP_Sync_Owner_Data',  
        'api/APP_Get_Owner_Data',
        'api/APP_Update_Owner_Data',      
        'api/APP_Push_Token', 
        'api/APP_FBID',
        'api/APP_Main',
        'api/APP_Weather',
        'api/APP_Get_Main_Data',
        'api/APP_Add_Car_Data',
        'api/APP_Del_Car_Data',
        'api/APP_Get_News_Data',
        'api/APP_Get_News_Detail_Data',
        'api/APP_Get_Messages_Detail',
    ];
}
