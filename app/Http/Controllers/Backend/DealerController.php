<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services as Service;

class DealerController extends Controller
{
    //經銷商Service
    protected $dealerService;
    
    /**
    * 建構子
    *
    * @param Service\Backend\DealerService $dealerService
    */
    public function __construct(Service\Backend\DealerService $dealerService)
    {
        $this->dealerService = $dealerService;       
    }

    /**
    * 經銷商列表頁
    *
    * @param Request $request
    * @return void
    */
    public function index(Request $request)
    {  
        //預設有效
        if ( $_SERVER['REQUEST_METHOD'] == "GET" )
        {
            $request->valid = '1';
        }
        
        $tables = $this->dealerService->searchDealer($request,15);

        return view('admin.dealer.list', [
            'tables' => $tables,
            'cond' => $request,
        ]);
    }

    /**
    * 經銷商新增頁
    *
    * @param Request $request
    * @return void
    */
    public function create()
    {
        return view('admin.dealer.create',[
            'dealer' => "",
        ]);
    }

    /**
     * 經銷商修改頁
     *
     * @param integer $id
     * @return void
     */
    public function edit($id = 0)
    {
        $dealer = $this->dealerService->getDealer($id);

        return view('admin.dealer.edit',[
            'dealer' => $dealer,
        ]);
    }

    /**
     * 經銷商新增
     *
     * @param Request $request
     * @return void
     */
    public function insert(Request $request)
    {  
        $this->dealerService->insertDealer($request);

        return redirect('/backend/dealer');      
    }

    /**
     * 經銷商修改
     *
     * @param Request $request
     * @return void
     */
    public function modify(Request $request)
    {   
        $id = $this->dealerService->modifyDealer($request);
        
        return redirect('/backend/dealer/edit/' . $id);
    }

    /**
     * 經銷商刪除
     *
     * @param Request $request
     * @return void
     */
    public function delete($id = 0)
    {      
        $this->dealerService->deleteDealer($id);
        
        return redirect('/backend/dealer');
    }    
}
