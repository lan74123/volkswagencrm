<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services as Service;


/**
 * MemberController class
 * 
 */
class MemberController extends Controller
{   
    //會員Service
    protected $memberService, $excelService, $emailService, $carService;
    
    /**
    * 建構子
    *
    * @param Service\Common\MemberService $memberService
    */
    public function __construct(Service\Common\MemberService $memberService, Service\Common\ExcelService $excelService, 
    Service\Common\EmailService $emailService, Service\Common\CarService $carService)
    {
        $this->memberService = $memberService;
        $this->excelService = $excelService;
        $this->emailService = $emailService;
        $this->carService = $carService;
    }

    /**
    * 會員列表頁
    *
    * @param Request $request
    * @return void
    */
    public function index(Request $request)
    {  
        //預設有效
        if ( $_SERVER['REQUEST_METHOD'] == "GET" )
        {
            $request->valid = '1';
        }
        
        $tables = $this->memberService->searchMember($request,15);

        return view('admin.member.list', [
            'tables' => $tables,
            'cond' => $request,
        ]);
    }

    /**
    * 會員新增頁
    *
    * @param Request $request
    * @return void
    */
    public function create()
    {
        return view('admin.member.create',[
            'member' => "",
        ]);
    }

    /**
     * 會員修改頁
     *
     * @param integer $id
     * @return void
     */
    public function edit($id = 0)
    {
        $member = $this->memberService->getMember($id);

        if(is_null($member))
        {
            return redirect('/backend/member'); 
        }

        $cars = $this->carService->searchMemberCar(0, $id, null);
        foreach($cars as $car)
        {
            $car['maintains'] = $this->carService->getCarMaintains($car->origin_id ,$car->vin ,$car->license_plate_number);
        }
        
        return view('admin.member.edit',[
            'member' => $member,
            'mambercars' => $cars
        ]);
    }

    /**
     * 會員新增
     *
     * @param Request $request
     * @return void
     */
    public function insert(Request $request)
    {  
        $this->memberService->insertMember($request);

        return redirect('/backend/member');      
    }

    /**
     * 會員修改
     *
     * @param Request $request
     * @return void
     */
    public function modify(Request $request)
    {   
        $id = $this->memberService->modifyMember($request);

        return redirect('/backend/member/edit/'.$id);
    }

    /**
     * 會員刪除
     *
     * @param Request $request
     * @return void
     */
    public function delete($id = 0)
    {      
        $this->memberService->deleteMember($id);
        
        return redirect('/backend/member');
    }

    /**
     * 會員資料下載
     *
     * @return void
     */
    public function download(Request $request)
    {         
        try
        {                      
            $tables = $this->memberService->searchMember($request,0);

            $titles=[
                '會員編號' ,
                '姓名',
                'Email',
                '連絡電話',
                '連絡地址',
                '同意資料使用',
                '註冊日期',
                '狀態'
            ];
            
            $datas=array();

            foreach($tables as $row)
            {           
                $rowData=array();
                array_push($rowData,$row['id']);
                array_push($rowData,$row['name']);
                array_push($rowData,$row['email']);
                array_push($rowData,$row['mobile']);
                array_push($rowData,$row['address']);
                array_push($rowData,( $row['agree_data_use'] )? 'Y':'N');
                array_push($rowData,$row['created_at']);
                array_push($rowData,( $row['valid'] )? '有效':'無效');           
                array_push($datas,$rowData);
            }      

            $this->excelService->downloadExcel($titles, $datas, 'member_'.date("Y-m-d-h:i:s",time()));  

        }
        catch(Exception $e)
        {

            return redirect('/backend/member');
        }
    }
    

    /**
     * 會員新增車輛
     *
     * @param Request $request
     * @return void
     */
    public function insertCar($id = 0, Request $request)
    {         
        $msg = $this->carService->insertCar($id, $request);

        return redirect('/backend/member/edit/' . $id);      
    }

    /**
     * 會員刪除車輛
     *
     * @param Request $request
     * @return void
     */
    public function deleteCar($id = 0, Request $request)
    {   
        $msg = $this->carService->deleteCar($id, $request);

        return redirect('/backend/member/edit/' . $id);      
    }
    
}
