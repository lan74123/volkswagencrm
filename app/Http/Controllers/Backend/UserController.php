<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    protected $userService;

    /**
     * 建構子
     *
     * @param \App\Services\Backend\UserService $userService
     */
    public function __construct(\App\Services\Backend\UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * 列表
     *
     * @return void
     */
    public function index()
    {
        $users = $this->userService->getAllUsers();

        $viewData = [
            'users' => $users,
        ];

        return view('admin.user.list', $viewData);
    }

    /**
     * 建立
     *
     * @return void
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * 編輯
     *
     * @param [type] $id
     * @return void
     */
    public function edit($id)
    {
        $user = $this->userService->getUser($id);

        $viewData = [
            'user' => $user
        ];

        return view('admin.user.edit', $viewData);
    }

    /**
     * 更新
     *
     * @param Request $request
     * @param [type] $userId
     * @return void
     */
    public function update(Request $request, $userId)
    {
        $this->userService->modifyUser($request, $userId);

        return redirect('/backend/user/' . $userId);
    }

    /**
     * 儲存
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $userId = $this->userService->insertUser($request);

        return redirect('/backend/user/' . $userId);
    }

    /**
     * 刪除
     *
     * @param [type] $userId
     * @return void
     */
    public function delete($userId)
    {
        $this->userService->deleteUser($userId);

        return redirect()->back();
    }
}
