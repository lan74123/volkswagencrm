<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services as Service;

class StoreController extends Controller
{
     //商店Service
     protected $storeService, $dealerService;
    
     /**
      * 建構子
      *
      * @param Service\Backend\StoreService $storeService
      * @param Service\Backend\DealerService $dealerService
      */
     public function __construct(Service\Backend\StoreService $storeService, Service\Backend\DealerService $dealerService)
     {
         $this->storeService = $storeService;      
         $this->dealerService = $dealerService; 
     }
 
     /**
     * 商店列表頁
     *
     * @param Request $request
     * @return void
     */
     public function index($dealer_id = null, Request $request)
     {  
        if(!empty($dealer_id))
        {   
            $request->dealer = $dealer_id;
            $request->allplace = false;  
            $request->disabled = '';         
        }
        else
        {           
            $request->allplace = false;  
            $request->disabled = '';           
        }
        //預設有效
        if ( $_SERVER['REQUEST_METHOD'] == "GET" )
        {
            $request->valid = '1';
        }

        $tables = $this->storeService->searchStore($request, 15);        

        return view('admin.store.list', [
             'tables' => $tables,
             'cond' => $request,
             'dealerId' => $dealer_id,
         ]);
     }
 
     /**
     * 商店新增頁
     *
     * @param Request $request
     * @return void
     */
     public function create($dealer_id = null, Request $request)
     {
        $dealer = $this->dealerService->getDealer($dealer_id);

        $request->dealer = '';
        $request->allplace = true;
        $request->disabled = '';
        $request->url = '';

        return view('admin.store.create',[
            'store' => "",
            'dealerId' => $dealer_id,
            'cond' => $request,
            'dealer' => $dealer,
        ]);
     }
 
     /**
      * 商店修改頁
      *
      * @param integer $id
      * @return void
      */
     public function edit(int $dealer_id, $id = 0)
     {  
        if(empty($dealer_id))
        {
            return redirect('/backend/store'); 
        }

        $store = $this->storeService->getStore($id);       
 
        return view('admin.store.edit',[
            'store' => $store,
        ]);
     }
 
     /**
      * 商店新增
      *
      * @param Request $request
      * @return void
      */
     public function insert(Request $request)
     {  
         $this->storeService->insertStore($request);
 
         return redirect('/backend/store/' . $request->dealer);      
     }
 
     /**
      * 商店修改
      *
      * @param Request $request
      * @return void
      */
     public function modify(int $dealer_id, Request $request)
     {  
         $id = $this->storeService->modifyStore($request);
         
         return redirect('/backend/store/' . $dealer_id . '/edit/' . $id);
     }
 
     /**
      * 商店刪除
      *
      * @param Request $request
      * @return void
      */
     public function delete($id = 0)
     {      
         $this->storeService->deleteStore($id);
         
         return redirect('/backend/store');
     }    
}
