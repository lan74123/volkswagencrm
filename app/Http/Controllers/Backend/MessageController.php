<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services as Service;


/**
 * MessageController class
 * 
 */
class MessageController extends Controller
{   
    //訊息Service
    protected $messageService, $excelService, $emailService, $memberService;
    
    /**
     * 建構子
     *
     * @param Service\Backend\MessageService $messageService
     * @param Service\Common\ExcelService $excelService
     * @param Service\Common\EmailService $emailService
     */
    public function __construct(Service\Backend\MessageService $messageService, 
    Service\Common\ExcelService $excelService, 
    Service\Common\EmailService $emailService,
    Service\Common\MemberService $memberService)
    {
        $this->messageService = $messageService;
        $this->excelService = $excelService;
        $this->emailService = $emailService;
        $this->memberService = $memberService;
    }

    /**
    * 訊息列表頁
    *
    * @param Request $request
    * @return void
    */
    public function index(Request $request)
    {  
        $tables = $this->messageService->searchMessage($request,15);

        return view('admin.message.list', [
            'tables' => $tables,
            'cond' => $request,
        ]);
    }
    /**
    * 訊息新增頁
    *
    * @param Request $request
    * @return void
    */
    public function create(Request $request)
    {        

        return view('admin.message.create',[
            'message' => "",    
            'cond' => $request,        
        ]);
    }
    /**
     * 訊息新增
     *
     * @param Request $request
     * @return void
     */
    public function insert(Request $request)
    {        
        $this->messageService->insertMessage($request);       

        return redirect('/backend/message');      
    }

    
    /**
     * 訊息修改頁
     *
     * @param integer $id
     * @return void
     */
    public function edit($id = 0)
    {

        if($id == 0)
        {
            return redirect('/backend/message');
        }

        $message = $this->messageService->getMessage($id);
 
        return view('admin.message.edit',[
            'message' => $message
        ]);
    }

    /**
     * 訊息修改
     *
     * @param Request $request
     * @return void
     */
    public function modify(Request $request)
    {   
        $id = $this->messageService->modifyMessage($request);

        return redirect('/backend/message/edit/'.$id);
    }

    /**
     * 訊息刪除
     *
     * @param Request $request
     * @return void
     */
    public function delete($id = 0)
    {      
        //刪除群組
        $this->messageService->deleteMessage($id);       

        return redirect('/backend/message');
    }

    public function resend($id = 0)
    { 
         //重發推播
        $this->messageService->findPreSendMessageById($id);  
    

         return redirect('/backend/message');
    }
    

   
}
