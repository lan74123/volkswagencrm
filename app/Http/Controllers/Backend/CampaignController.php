<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CampaignController extends Controller
{
    protected $campaignService;

    /**
     * 建構子
     *
     * @param \App\Service\Backend\CampaignService $campaignService
     */
    public function __construct(\App\Services\Backend\CampaignService $campaignService)
    {
        $this->campaignService = $campaignService;
    }

    /**
     * 列表
     *
     * @return void
     */
    public function index()
    {
        $campaigns = $this->campaignService->getAllCampaigns();

        $viewData = [
            'campaigns' => $campaigns,
        ];

        return view('admin.campaign.list', $viewData);
    }

    /**
     * 建立
     *
     * @return void
     */
    public function create()
    {
        return view('admin.campaign.create');
    }

    /**
     * 編輯
     *
     * @param integer $id
     * @return void
     */
    public function edit($id = 0)
    {
        $campaign = $this->campaignService->getCampaign($id);

        $viewData = [
            'campaign' => $campaign,
        ];

        return view('admin.campaign.edit', $viewData);
    }

    /**
     * 更新
     *
     * @param Request $request
     * @param [type] $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        $this->campaignService->modifyCampaign($request, $id);

        return redirect('/backend/campaign/' . $id);
    }

    /**
     * 儲存
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $campaignId = $this->campaignService->insertCampaign($request);

        return redirect('/backend/campaign/' . $campaignId);
    }

    /**
     * 刪除
     *
     * @param [type] $id
     * @return void
     */
    public function delete($id)
    {
        $this->campaignService->deleteCampaign($id);

        return redirect()->back();
    }
}
