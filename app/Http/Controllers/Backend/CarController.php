<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Services as Service;

use Illuminate\Http\Request;
use Cookie;

class CarController extends Controller
{
     //Service
     protected $memberService, $carService;
    
     /**
      * 建構子
      *
      * @param Service\Common\MemberService $memberService
      * @param Service\Common\CarService $carService
      */
     public function __construct(Service\Common\MemberService $memberService, Service\Common\CarService $carService)
     {
         $this->memberService = $memberService;       
         $this->carService = $carService;
     }

     /**
    * 車輛列表頁
    *
    * @param Request $request
    * @return void
    */
    public function index(Request $request)
    {   
        if($_SERVER['REQUEST_METHOD'] == "GET")
        {
            $request->name = ($request->cookie('b_car_name')) ? $request->cookie('b_car_name') : $request->name;
            $request->vin = ($request->cookie('b_car_vin')) ? $request->cookie('b_car_vin') :  $request->vin;
            $request->license_plate_number = ($request->cookie('b_car_license_plate_number')) ? $request->cookie('b_car_license_plate_number') : $request->license_plate_number;
        }
        else
        {
            Cookie::queue('b_car_name', $request->name.'', 60);
            Cookie::queue('b_car_vin', $request->vin, 60);
            Cookie::queue('b_car_license_plate_number', $request->license_plate_number, 60);            
        }
     
        if( is_null($request->name) && is_null($request->vin) && is_null($request->license_plate_number))
        {
            $tables = [];
        }
        else
        {
            $tables = $this->carService->searchCarOriginForBackend(20, $request);
        }       
      
        return view('admin.car.list', [
            'tables' => $tables,
            'cond' => $request,
        ]);
    }


}
