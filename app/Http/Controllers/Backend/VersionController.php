<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services as Service;

class VersionController extends Controller
{
    //Version Service
    protected $versionService;
    
    /**
    * 建構子
    *
    * @param Service\Backend\VersionService $versionService
    */
    public function __construct(Service\Backend\VersionService $versionService)
    {
        $this->versionService = $versionService;       
    }

    /**
    * Version 列表頁
    *
    * @param Request $request
    * @return void
    */
    public function index(Request $request)
    {  
        
        $tables = $this->versionService->searchVersion($request,15);

        return view('admin.version.list', [
            'tables' => $tables,
            'cond' => $request,
        ]);
    }

    /**
    * Version 新增頁
    *
    * @param Request $request
    * @return void
    */
    public function create()
    {
        return view('admin.version.create',[
            'version' => "",
        ]);
    }

    /**
     * Version 修改頁
     *
     * @param integer $id
     * @return void
     */
    public function edit($id = 0)
    {
        $version = $this->versionService->getVersion($id);

        return view('admin.version.edit',[
            'version' => $version,
        ]);
    }

    /**
     * Version 新增
     *
     * @param Request $request
     * @return void
     */
    public function insert(Request $request)
    {  
        $this->versionService->insertVersion($request);

        return redirect('/backend/version');      
    }

    /**
     * Version 修改
     *
     * @param Request $request
     * @return void
     */
    public function modify(Request $request)
    {   
        $id = $this->versionService->modifyVersion($request);
        
        return redirect('/backend/version/');
    }

    /**
     * Version 刪除
     *
     * @param Request $request
     * @return void
     */
    public function delete($id = 0)
    {      
        $this->versionService->deleteVersion($id);
        
        return redirect('/backend/version');
    }    
}
