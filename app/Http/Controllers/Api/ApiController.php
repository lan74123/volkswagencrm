<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Presenter\MessagePresenter;
use App\Services as Service;
use Validator;
use DB;

class ApiController extends Controller
{
    //Service
    protected $memberService, $carService, $newsService, $messageService, $devicePushService;

    /**
    * 建構子
    * @param Services\Common\MemberService $memberService
    * @param Services\Common\CarService $carService
    * @param Services\Common\NewsService $newsService
    * @param Services\Backend\VersionService $versionService
    * @param Services\Backend\DealerService $dealerService
    * @param Service\Backend\MessageService $messageService
    */
    public function __construct(Service\Common\MemberService $memberService, 
    Service\Common\DevicePushService $devicePushService, 
    Service\Common\CarService $carService,
    Service\Common\NewsService $newsService,
    Service\Backend\VersionService $versionService,
    Service\Backend\DealerService $dealerService,
    Service\Backend\MessageService $messageService)
    {
        $this->middleware('api', ['except' => ['login']]);

        $this->memberService = $memberService;
        $this->carService = $carService;
        $this->newsService = $newsService;
        $this->versionService = $versionService;
        $this->dealerService = $dealerService;
        $this->messageService = $messageService;
        $this->devicePushService = $devicePushService;
    }   

    /** test */
    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth('api')->attempt($credentials)) 
        {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        // return $this->respondWithToken($token);
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'check_valid' => auth('api')->check(),
        ]);
    }

    /** test */
    public function test(Request $request)
    {
        // $data['user_data'] = auth()->user(); // get user data by token
        $data['user_id'] = auth('api')->user()->id; // get user data by token
        $data['token'] = auth('api')->getToken(); // get current token
        // $data['invalid'] = auth()->invalidate(); // invalidate token;
        // $data['check'] = auth()->check(); // check token valid;
        //$data['refresh_token'] = auth()->refresh(); //refresh token
        // $data['check_after_refresh'] = auth()->check(); // check token valid;
    }

    /**
     * 版本控制
     * @param Request $request
     */
    public function Version(Request $request)
    {
        $device = (is_null($request->input('Device')))? '' : strtolower($request->input('Device'));
        $version = $this->versionService->getLatestVersion($device);

        if ($version != null)
        {
            return response()->json([
                'Result' => 'Y',
                'ErrorCode' => '',
                'ErrorMsg' => '',
                'Version' => $version->version,
                'IsUpdate' => $version->is_update == 1 ? 'Y' : 'N',
                'Message' => $version->message,
                'URL' => $version->url,
            ],200);
        }
        else
        {
            return response()->json([
                'Result' => 'N',
                'ErrorCode' => '',
                'ErrorMsg' => '參數不足',
                'Version' => '',
                'IsUpdate' => '',
                'Message' => '',
                'URL' => '' 
            ],200);
        }
    }

    /**
     * 車主註冊
     * @param Request $request
     */
    public function SignUp(Request $request)
    {
        //FIXME:DeviceID其實不用存下來,因為有開另外一張表了   
        $DeviceID = $request->input('DeviceID');

        if(is_null($DeviceID))
        {
            $msg = '缺少 裝置ID';

            return response()->json([
                'Result' => 'N',
                'ErrorCode' => '',
                'ErrorMsg' => $msg
            ],200);   
        }

        $data = $this->memberService->registerMember($request);
        $msg = $data['msg'];
        $member = $data['member'];

        if($msg == '車輛加入成功' || $msg == '超過車輛上限:3' || $msg == '車輛已存在')
        {

            //註冊成功 綁定DEVICEID 跟 會員ID
            $devicePush = $this->devicePushService->getDevicePush($DeviceID); 
            
            if(!is_null($devicePush))           
            {
                $devicePush = $this->devicePushService->modifyDevicePush($devicePush->id, $member->id, null);
            }

            $token = auth('api')->tokenById($member->id);

            return response()->json([
                'Result' => 'Y',
                'ErrorCode' => '',
                'ErrorMsg' => '',
                'UserToken' => $token,
            ],200);
        }
        else
        {
            return response()->json([
                'Result' => 'N',
                'ErrorCode' => '',
                'ErrorMsg' => $msg,
                'UserToken' => '',
            ],200);
        }       
    }

    public function VwidLogin(Request $request)
    {
        $id_token = $request->input('id_token');
        $auth_code = $request->input('auth_code');

        return response()->json([
            'Result' => 'Y',
            'ErrorCode' => '',
            'ErrorMsg' => '',
            'UserToken' => '',
        ],200);
    }

    /**
     * 帳號自動登入
     * @param Request $request
     */
    public function MemberLogin(Request $request)
    {        
        $name = auth('api')->user()->name;
        //get name from token

       /*  
        FIXME:
        這裡不能用name做唯一參考 會找出同名同姓的人
        應用 name+VIN+license_plate_number
        img_path好像沒有用到了可去除 待確認
        */
        if(is_null($name))
        {
            return response()->json([
                'Result' => 'N',
                'ErrorCode' => '',
                'ErrorMsg' => '缺少車主姓名',
                'UserToken' => '',           
                //'UserData' => '' //put array here
            ],200);
        }
        
        $data = $this->memberService->memberLogin($name);
        $msg = $data['msg'];
        $member = $data['member'];

        $UserData['name'] = $member->name;
        $UserData['vin'] = $member->vin;
        $UserData['license_plate_number'] = $member->license_plate_number;  
        $UserData['img_path'] = $member->img_path;            

        return response()->json([
            'Result' => 'Y',
            'ErrorCode' => '',
            'ErrorMsg' => $msg,
            'UserToken' => auth('api')->getToken()->get(),  
            'UserData' => $UserData,
        ],200 );
    }
    
    /**
     * 取得帳號資料
     * @param Request $request
     */
    public function GetOwnerData(Request $request)
    {  
        $UserID = auth('api')->user()->id;
        $UserToken = auth('api')->getToken()->get();
       // $UserName = $request->input('UserName');
       // $UserPhone = $request->input('UserPhone');

        $member = $this->memberService->getMember($UserID);       

        $UserData['img_path'] = $member->img_path;
        $UserData['name'] = $member->name;
        $UserData['mobile'] = $member->mobile;
        $UserData['email'] = $member->email;       
        $UserData['address'] = $member->address;
        $UserData['fbid'] = $member->fbid;

        return response()->json([
            'Result' => 'Y',
            'ErrorCode' => '',
            'ErrorMsg' => '',
            'UserToken' => $UserToken,
            'UserData' => $UserData
        ],200);
    }
    
    /**
     * 修改帳號資料
     * @param Request $request
     */
    public function UpdateOwnerData(Request $request)
    {
        $UserID = auth('api')->user()->id;
        $UserEmail = $request->input('UserEmail');
        $UserPhone = $request->input('UserPhone');
        $UserAddress = $request->input('UserAddress');

        $data = $this->memberService->updateMember($UserID, $UserEmail, $UserPhone, $UserAddress, null, 'mb');
        $member = $data['member'];

        if(!is_null($member))
        {
            return response()->json([
                'Result' => 'Y',
                'ErrorCode' => '',
                'ErrorMsg' => ''
            ],200);
        }
        else
        {
            return response()->json([
                'Result' => 'N',
                'ErrorCode' => '',
                'ErrorMsg' => $data['msg']
            ],200);
        }
    }

    /**
     * 更新Facebook ID
     * @param Request $request
     */
    public function UpdateFBID(Request $request)
    {
        //FIXME:這邊沒有用到DeviceID應可去除
        $UserID = auth('api')->user()->id;
        $DeviceID = $request->input('DeviceID');
        $FBID = $request->input('FBID');
        $Type = strtoupper($request->input('Type'));

        if($Type == 'B')
        {
            if(is_null($FBID))
            {
                return response()->json([
                    'Result' => 'N',
                    'ErrorCode' => '',
                    'ErrorMsg' => '需綁定FBID'
                ],200);
            }
        }
        else if ($Type == 'R')
        {
            $FBID = '';
        }
        else
        {
            return response()->json([
                'Result' => 'N',
                'ErrorCode' => '',
                'ErrorMsg' => '參數不足'
            ],200);
        }

        $data = $this->memberService->updateMember($UserID, null, null, null, $FBID, 'fb');
        $member = $data['member'] ;

        if(!is_null($member))
        {
            return response()->json([
                'Result' => 'Y',
                'ErrorCode' => '',
                'ErrorMsg' => ''
            ],200);
        }
        else
        {
            return response()->json([
                'Result' => 'N',
                'ErrorCode' => '',
                'ErrorMsg' => '參數不足'
            ],200);
        }
    }
    
    /**
     * Push Token更新
     * @param Request $request
     */
    public function PushToken(Request $request)
    {                
        $DeviceID = $request->input('DeviceID');
        $PushToken = $request->input('PushToken');
        $Device = $request->input('Device');

        if(is_null($DeviceID))
        {
            $msg = '缺少 裝置ID';

            return response()->json([
                'Result' => 'N',
                'ErrorCode' => '',
                'ErrorMsg' => $msg
            ],200);            
        }        

        if(is_null($Device))
        {
            $msg = '缺少 裝置';

            return response()->json([
                'Result' => 'N',
                'ErrorCode' => '',
                'ErrorMsg' => $msg
            ],200);    
        }

        $devicePush = $this->devicePushService->getDevicePush($DeviceID);        

        if(is_null($devicePush))
        {
            $devicePush = $this->devicePushService->insertDevicePush($DeviceID, $PushToken, $Device);
        }
        else
        {
            $devicePush = $this->devicePushService->modifyDevicePush($devicePush->id, null, $PushToken);
        }

        return response()->json([
            'Result' => 'Y',
            'ErrorCode' => '',
            'ErrorMsg' =>  ''
        ],200);
    }

    

    /**
     * 取得車輛資料
     * @param Request $request
     */
    public function GetMainData(Request $request)
    {
        $UserToken = auth('api')->getToken()->get();
        $UserID = auth('api')->user()->id;
        // $UserID = 3;
        $member = $this->memberService->getMember($UserID);
        $cars = $this->carService->searchMemberCar(0, $UserID, null, 'asc');
        $MessageDatas = $this->messageService->getMessageByMember(0, $UserID);
        
        $myMember['Name'] = $member->name;
        $myMember['Mobile'] = (is_null($member->mobile))? '' : $member->mobile;
        $myMember['Email'] =  (is_null($member->email))? '' : $member->email; 
        $myMember['Address'] =  (is_null($member->address))? '' : $member->address;
        $myMember['img_path'] =  (is_null($member->img_path))? '' : $member->img_path;

        $final = [];
       
        foreach($cars as $car)
        {
            $data = [];
            $data['VIN'] = $car->vin;
            $data['CarData'] = [];
            $data['CarOwner'] = $myMember;
            $data['CarMaintainData'] = [];
            $data['MessageData'] = [];
            $MessageDatas = $this->messageService->getMessageByMember(0, $UserID);
            foreach($MessageDatas as $MessageData)
            {
                $temparray3['ID'] = $MessageData->id;
                $temparray3['title'] = $MessageData->title;
                $temparray3['desc'] = $MessageData->content;
                $temparray3['status'] = $MessageData->status;
                array_push($data['MessageData'],$temparray3);
            }
            
            $temparray2['VIN'] = $car->vin;
            $temparray2['LicensePlateNumber'] = $car->license_plate_number;
            $temparray2['CarModel'] = (is_null($car->car_model))? '' : $car->car_model;
            $temparray2['CarType'] = (is_null($car->car_type))? '' : $car->car_type;
            try
            {
                $timeCarDate = strtotime($car->car_listing_date);
                $CarDate = date('Y/m/d',$timeCarDate);
            }
            catch(\Throwable  $e)
            {
                $CarDate = '';
            }
            $temparray2['CarDate'] = $CarDate;
            try
            {
                $timeOwnerDate = strtotime($car->member_listing_date);
                $OwnerDate = date('Y/m/d',$timeOwnerDate);
            }
            catch(\Throwable  $e)
            {
                $OwnerDate = '';
            }
            $temparray2['OwnerDate'] = $OwnerDate;

            $temparray2['Mileage'] = $car->mileage;
            $dealer = $this->dealerService->getDealer($car->dealer_id);

            if(!is_null($dealer))
            {
                $temparray2['Dealer'] = $dealer->name;
            }
            else
            {
                $temparray2['Dealer'] = '';
            }
           
            //單筆的車子資料紀錄 in temparray2
            array_push($data['CarData'],$temparray2);

            $car['maintains'] = $this->carService->getCarMaintains($car->origin_id ,$car->vin ,$car->license_plate_number);
            foreach($car['maintains'] as $maintain)
            {
                $temparray['VIN'] = $maintain->vin;
                $temparray['LicensePlateNumber'] = $maintain->license_plate_number;
                $temparray['Dealer'] = $maintain->dealer;
                $temparray['Charge'] = $maintain->charge;
                $temparray['Mileage'] = $maintain->mileage;
                $temparray['CloseDate'] = $maintain->close_date;
                //單筆的車子保養紀錄 in temparray
                array_push($data['CarMaintainData'],$temparray);
                
            }
            array_push($final,$data);
        }
        
        if ($final != null)
        {
            return response()->json([
                'Result' => 'Y',
                'ErrorCode' => '',
                'ErrorMsg' => '',
                'UserToken' => $UserToken,
                'datas' => $final,
            ],200);
        }
        else
        {
            return response()->json([
                'Result' => 'N',
                'ErrorCode' => '',
                'ErrorMsg' => '參數不足',
                'UserToken' => '',
                'datas' => '',
            ],200);
        }

    }
    
    /**
     * 新增車輛資料
     * @param Request $request
     */
    public function AddCarData(Request $request)
    {
        $request->request->add(['name' => auth()->user()->name]);
        $resultMessage = $this->carService->insertCar(auth()->user()->id, $request);

        if($resultMessage == '車輛加入成功')
        {
            $result = 'Y';
        }
        else
        {
            $result = 'N';
        }

        return response()->json([
            'Result' => $result,
            'ErrorCode' => '',
            'ErrorMsg' => $resultMessage
        ],200);
    }
    
    /**
     * 刪除車輛資料
     * @param Request $request
     */
    public function DelCarData(Request $request)
    {
        // $UserToken = $request->input('UserToken');
        // $VIN = $request->input('VIN');
        // $LicensePlateNumber = $request->input('LicensePlateNumber');
        $request->request->add(['name' => auth()->user()->name]);
        $resultMessage = $this->carService->deleteCar(auth()->user()->id, $request);

        if($resultMessage == '車輛移除成功')
        {
            $result = 'Y';
        }
        else
        {
            $result = 'N';
        }

        return response()->json([
            'Result' => $result,
            'ErrorCode' => '',
            'ErrorMsg' => $resultMessage
        ],200);
    }
    
    /**
     * 取得最新消息
     * @param Request $request
     */
    public function GetNewsData(Request $request)
    {
        $NewsType = $request->input('NewsType');  

        if(!is_null($NewsType))
        {
            $NewsAll = $this->newsService->searchPublicNews(0, $NewsType, null);
            
            return response()->json([
                'Result' => 'Y',
                'ErrorCode' => '',
                'ErrorMsg' => '',
                'NewsData' => $NewsAll,
            ],200);
        }
        else
        {
            return response()->json([
                'Result' => 'N',
                'ErrorCode' => '',
                'ErrorMsg' => '消息類別錯誤',
                'NewsData' => '',
            ],200);
    
        }
    }
    
    /**
     * 取得最新/活動消息 內文
     * @param Request $request
     */
    public function GetNewsDetailData(Request $request)
    {
        //FIXME:錯誤訊息格式錯誤 未輸入訊息ID時
        $NewsID = $request->input('NewsID');

        $validateRules = [
            'NewsID' => 'required|max:100',          
        ];

        $validateMessage = [           
            'NewsID.required' => MessagePresenter::getRequired('有效ID','text'),                    
        ];

        $request->validate($validateRules, $validateMessage);         

        $News = $this->newsService->searchPublicNews(1, null, $NewsID);
       
        return response()->json([
            'Result' => 'Y',
            'ErrorCode' => '',
            'ErrorMsg' => '',
            'NewsData' => $News,
        ],200);
    }
    
    /**
     * 取得訊息 內文 紀錄已讀狀態記錄by User
     * @param Request $request
     */
    public function GetMessagesDetail(Request $request)
    {
        $UserID = auth('api')->user()->id;
        $MessageID = $request->input('MessageID');        
        
        $messageTemp = $this->messageService->findMessageByID($MessageID, $UserID);

        if(!is_null($messageTemp))
        {
            $message['MessageID'] = $messageTemp->id;
            $message['Title'] = $messageTemp->title;
            $message['Content'] = $messageTemp->content;
            $message['ImageURL'] = $messageTemp->img_path;
            $message['url'] = $messageTemp->url;
            $message['Cdate'] = date_format($messageTemp->created_at, 'Y-m-d H:i:s');
            $message['Mdate'] = date_format($messageTemp->updated_at, 'Y-m-d H:i:s');

            return response()->json([
                'Result' => 'Y',
                'ErrorCode' => '',
                'ErrorMsg' => '',
                'data' => $message
            ],200);
        }
        else
        {
            return response()->json([
                'Result' => 'N',
                'ErrorCode' => '',
                'ErrorMsg' => '取得訊息失敗',
                'data' => '',
            ],200);
        }
    }   
   
    /**
     * 取得主頁
     *
     * @param Request $request
     * @return void
     */
    public function APPMain(Request $request)
    {
        $UserToken = auth('api')->getToken()->get();       
        $UserID = auth('api')->user()->id;
        $memberCars =  $this->carService->searchMemberCar(0, $UserID, null, 'desc');

        $data['CarMaintainData']= [];

        foreach($memberCars as $memberCar)
        {
            if(count($data['CarMaintainData']) == 0)
            {
                $temparray['LicensePlateNumber'] = $memberCar->license_plate_number;
                $temparray['Mileage'] = $memberCar->mileage;                
                try
                {
                    $time = strtotime($memberCar->visit_date);
                    $newformat = date('Y/m/d',$time);
                }
                catch(\Throwable  $e)
                {
                    $newformat = '';
                }
                $temparray['visit_date'] = $newformat;
               
                array_push($data['CarMaintainData'],$temparray);
            }
        }

        $data['NewsData'] = $this->newsService->searchPublicNews(2, 1, null);
        
        $data['EventsData'] = $this->newsService->searchPublicNews(2, 2, null);

        return response()->json([
            'Result' => 'Y',
            'ErrorCode' => '',
            'ErrorMsg' => '',
            'UserToken' => $UserToken,
            'datas' => $data,
            //'UserData' => '' //put array here
        ],200);

    }

    /**
     * 取得天氣資訊
     * @param Request $request
     */
    public function Weather(Request $request)
    {
        return response()->json([
            'Result' => 'Y',
            'ErrorCode' => '',
            'ErrorMsg' => '',
            'datas' =>
            [
                'temperature' =>'25度',
                'weather' => '雨天',
            ]
        ],200);     
    }
}
