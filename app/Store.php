<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    public function dealer()
    {
        return $this->hasOne('App\Dealer', 'id', 'dealer_id');
    }
}
