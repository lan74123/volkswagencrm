<?php

namespace App\Repository\Common;

use Illuminate\Http\Request;
use App\Maintain;
use App\CarOrigin;
use App\CarDelete;
use App\Car;
use Exception;
use Auth;
use App;
use DB;


class CarRepository
{

    /**
     * 搜尋會員車輛
     *
     * @param integer $pageLimit
     * @param [type] $name
     * @param [type] $mobile
     * @param string $date_start
     * @param string $date_end
     * @param [type] $valid
     * @return void
     */
    public function searchMemberCars($pageLimit = 0, $member_id = 0, $car_id = 0, $sort = 'desc')
    {        
        $Car_All = Car::select('*');

        if(!is_null($member_id))
		{  
            $Car_All->where('member_id', $member_id);            
        }	

        if(!is_null($car_id))
		{  
            $Car_All->where('id', $car_id);            
        }
        
        $Car_All->where('valid', 1);

        $Car_All->orderBy('created_at', $sort)->limit(3);

        //GetAll
        if( $pageLimit == 0 )
        {         
            $Car_All = $Car_All->get();
        }
        else
        {
            $Car_All = $Car_All->paginate($pageLimit);
        }
            
        return $Car_All;       
    }

    /**
     * 從車輛表找車
     *
     * @param integer $pageLimit
     * @param integer $car_id
     * @return void
     */
    public function searchCar($member_id = 0, $vin = null, $license_plate_number = null)
    {       
        $Car = Car::where('member_id', $member_id)->where('vin','like', '%' . $vin)
        ->where('license_plate_number', $license_plate_number)->orderBy('updated_at', 'desc')->first(); 

        return $Car;
    }

    /**
     * 新增車輛
     *
     * @param integer $id
     * @return void
     */
    public function insertCar($member_id = 0, $car_origin = null)
    {
        $car = new Car;
        $car->member_id = $member_id;
        $car->car_type_id = $member_id;
        $car->origin_id = $car_origin->origin_id;        

        $car->vin = $car_origin->vin;        
        $car->license_plate_number = $car_origin->license_plate_number;
        $car->car_model = $car_origin->car_model;
        $car->car_type = $car_origin->car_type;
        $car->car_listing_date = $car_origin->car_listing_date;  
        $car->member_listing_date = $car_origin->member_listing_date;      
        $car->mileage = $car_origin->mileage;        
        $car->visit_date = $car_origin->visit_date;
        $car->dealer_id = $car_origin->dealer_id;
        $car->dealer = $car_origin->dealer;
        $car->valid = 1;
        $car->oid = 1;
        $car->save();
        $id = $car->id;

        return $id;
    }
     
     /**
     * 後台從車輛比對表找尋資料
     *
     * @param integer $pageLimit
     * @param integer $car_id
     * @return void
     */
    public function searchCarOriginForBackend($pageLimit = 0, $name = null, $vin = null, $license_plate_number = null)
    {       
        $CarOrigin = CarOrigin::select('*');

        if(!is_null($name))
		{			
            $CarOrigin->where(DB::raw('concat(COALESCE(name,"") , COALESCE(last_name,""))'), 'like' , '%' . $name . '%');
        }  

        if(!is_null($vin))
		{			
            $CarOrigin->where('vin','like', '%' . $vin);
        } 

        if(!is_null($license_plate_number))
		{			
            $CarOrigin->where('license_plate_number', $license_plate_number);
        } 

        $CarOrigin->orderBy('updated_at', 'desc')->orderBy('name', 'desc');

        //GetAll
        if( $pageLimit == 0 )
        {         
            $CarOrigin = $CarOrigin->get();
        }
        else
        {
            $CarOrigin = $CarOrigin->paginate($pageLimit);
        }    
       
        return $CarOrigin;
    }
    
    /**
     * 從車輛比對表找尋資料
     *
     * @param integer $pageLimit
     * @param integer $car_id
     * @return void
     */
    public function searchCarOrigin($name = null, $vin = null, $license_plate_number = null)
    {       
        $CarOrigin = CarOrigin::where(DB::raw('concat(COALESCE(name,"") , COALESCE(last_name,""))'), 'like' , '%' . $name . '%')
        ->where('vin','like', '%' . $vin)
        ->where('license_plate_number', $license_plate_number)->orderBy('updated_at', 'desc')->first(); 

        return $CarOrigin;
    }

    /**
     * 更新車輛比對表
     *
     * @param integer $id
     * @param integer $status 1:已新增 0:未新增
     * @return void
     */
    public function editCarOrigin($member_name = null, $car_origin = null, $status = 0)
    {       
        $CarOrigin = CarOrigin::find($car_origin->id);
        $CarOrigin->status = $status;
        $CarOrigin->save();               
    }

    /**
     * 新增刪除車輛到刪除表
     *
     * @param [type] $member_name
     * @param [type] $Car
     * @return void
     */
    public function insertDeleteCar($id = null)
    {
        $Car = Car::find($id);
        
        //移除車輛資料
        $Car_delete = new CarDelete;
        $Car_delete->member_id = $Car->member_id;
        $Car_delete->car_type_id = $Car->car_type_id;
        $Car_delete->origin_id = $Car->origin_id;

        $Car_delete->vin = $Car->vin;        
        $Car_delete->license_plate_number = $Car->license_plate_number;
        $Car_delete->car_model = $Car->car_model;
        $Car_delete->car_type = $Car->car_type;
        $Car_delete->car_listing_date = $Car->car_listing_date;  
        $Car_delete->member_listing_date = $Car->member_listing_date;      
        $Car_delete->mileage = $Car->mileage;        
        $Car_delete->visit_date = $Car->visit_date;
        $Car_delete->dealer_id = $Car->dealer_id;
        $Car_delete->dealer = $Car->dealer;
        $Car_delete->valid = 1;

        $Car_delete->save();
    }

    /**
     * 刪除車輛
     *
     * @param [type] $member_name
     * @param [type] $Car
     * @return void
     */
    public function deleteCar($id = null)
    {
        $Car = Car::find($id);
        $Car -> delete();
    }   

    public function getCarMaintains($origin_id = 0, $vin = 0, $license_plate_number = 0)    
    {
        $Maintain_All = Maintain::where('origin_id', $origin_id)->where('vin', $vin)
        ->where('license_plate_number', $license_plate_number)->orderBy('close_date', 'desc')->get(); 

        return $Maintain_All;

    }


    /**
     * 會員新增車輛
     *
     * @param [type] $car_id
     * @param [type] $car_origin_id
     * @return void
     */
    public function memberInsertCar($member_name = null, $member_id = null, $car_origin = null)
    {        
            $datas['member_id'] = $member_id;
            $datas['car_origin'] = $car_origin;
            $datas['member_name'] = $member_name;

            $result = '';  
                try
                {
                    DB::transaction(function () use ($datas) 
                    {
                        $member_id = $datas['member_id'];
                        $car_origin = $datas['car_origin'];
                        $member_name = $datas['member_name'];
                        //新增            
                        $id = $this->insertCar($member_id, $car_origin);

                        //更新比對資料庫被加入車輛
                        $this->editCarOrigin($member_name, $car_origin, 1); 
                                       
                    });
                    $result = 'Y';
                }  
                catch(\Throwable  $e)
                {
                    $result =  $e->getMessage();
                }
          
        return $result;

    } 

    /**
     * 會員刪除車輛
     *
     * @param [type] $car_id
     * @param [type] $car_origin_id
     * @return void
     */
    public function memberDeleteCar($member_name = null, $car_id = null, $car_origin = null)
    {
            
            $datas['car_id']=$car_id;
            $datas['car_origin']=$car_origin;
            $datas['member_name'] = $member_name;

            $result =  '';
                try
                {
                   DB::transaction(function () use ($datas) 
                    {
                        $car_id = $datas['car_id'];
                        $car_origin = $datas['car_origin'];
                        $member_name = $datas['member_name'];

                        //新增車輛刪除資料表
                        $this->insertDeleteCar($car_id);

                        //刪除車輛
                        $this->deleteCar($car_id);
                
                        //更新比對資料庫被加入車輛
                        $this->editCarOrigin($member_name, $car_origin, 0); 

                    });
                    $result = 'Y';
                }          
                catch(\Throwable  $e)
                {
                    $result =  $e->getMessage();
                }
            
          
        return $result;

    }  
}
