<?php

namespace App\Repository\Backend;

use Illuminate\Http\Request;
use App\Dealer;
use Auth;
use App;
use DB;


class DealerRepository
{

    /**
    * 搜尋多筆經銷商
    *
    * @param integer $pageLimit
    * @param [type] $area
    * @param [type] $name
    * @param [type] $tel
    * @param [type] $valid
    * @return void
    */
    public function searchDealers($pageLimit = 0, $area = null, $name = null, $tel = null, $valid = null)
    {        
        $Dealer_All = Dealer::select('*');

        if(!is_null($area))
		{			
            $Dealer_All->where('area', 'LIKE', '%' . $area . '%');
        }

        if(!is_null($name))
		{			
            $Dealer_All->where('name', 'LIKE', '%' . $name . '%');
        }
        
        if(!is_null($tel))
		{			
            $Dealer_All->where('tel', 'LIKE', '%' . $tel . '%');
        }     
    
        if(!is_null($valid))
		{           
            if($valid != "")//FOR 不拘
            {
                $Dealer_All->where('valid', $valid);
            }
		}		

        $Dealer_All->orderBy('id', 'desc')->orderBy('valid', 'desc');

        //GetAll
        if( $pageLimit == 0 )
        {         
            $Dealer_List = $Dealer_All->get();
        }
        else
        {
            $Dealer_List = $Dealer_All->paginate($pageLimit);
        }
            
        return $Dealer_List;       
    }

    /**
     * 取得單筆經銷商By ID
     *
     * @param integer $id
     * @return void
     */
    public function getDealer($id = 0)
    {
        $Dealer = Dealer::find($id);

        return $Dealer;      
    }  

     /**
     * 新增經銷商
     *
     * @param integer $id
     * @return void
     */
    public function insertDealer(Request $request)
    {
        $dealer = new Dealer;
        $dealer->area = $request->area;
        $dealer->name = $request->name;
        $dealer->code = $request->code;        
        $dealer->tel = $request->tel;
        $dealer->fax = $request->fax;
        $dealer->address = $request->address;
        $dealer->email = $request->email; 
        $dealer->emailcc = $request->emailcc;
        $dealer->servicetime = $request->servicetime;     
        $dealer->valid = ($request->valid == 'on') ? 1 : 0;       
        $dealer->oid = Auth::user()->id;        

        $dealer->save();

        $id = $dealer->id;

        return $id;
    }

    /**
     * 修改經銷商資料
     *
     * @param integer $id
     * @return void
     */
    public function modifyDealer(Request $request)
    {
        $dealer = Dealer::find($request->id);
        $dealer->area = $request->area;
        $dealer->name = $request->name;
        $dealer->code = $request->code;        
        $dealer->tel = $request->tel;
        $dealer->fax = $request->fax;
        $dealer->address = $request->address;
        $dealer->email = $request->email;  
        $dealer->emailcc = $request->emailcc;
        $dealer->servicetime = $request->servicetime;
        $dealer->valid = ($request->valid == 'on') ? 1 : 0;
        $dealer->oid = Auth::user()->id;        

        $dealer->save(); 

        return $request->id;
    }

    /**
     * 刪除經銷商
     *
     * @param integer $id
     * @return void
     */
    public function deleteDealer($id = 0)
    {
        $dealer = Dealer::find($id);      
        $dealer->valid = 0; 

        $dealer->save();
        //$dealer->delete();
    }
}
