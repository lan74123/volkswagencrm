<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public function member()
    {
        return $this->hasOne('App\Member', 'id', 'member_id');
    }

    public function messageGroup()
    {
        return $this->hasOne('App\MessageGroup', 'id', 'message_groups_id');
    }
}
