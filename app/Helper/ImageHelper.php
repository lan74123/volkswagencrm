<?php

/**
 * Created by PhpStorm.
 * User: leocc.lan
 * Date: 2017/6/15
 * Time: 下午 06:27
 */
namespace App\Helper;

class ImageHelper
{
    public static function resizeImage($img,$picFinalWidth,$picFinalHeight)
    {

        $image =  $img ;
        $originalWidth =  $image -> width();
        $originalHeight =  $image -> height();

        if($originalWidth > $originalHeight)
        {
            $image-> resize($originalWidth*($picFinalHeight/$originalHeight), $picFinalHeight);
        }
        else
        {
            $image-> resize($picFinalWidth , $originalHeight*($picFinalWidth/$originalWidth));
        }

        $widthAfter = $image -> width();
        $HeightAfter = $image -> height();

        //check2
        if($widthAfter<$picFinalWidth)
        {
            $image-> resize($picFinalWidth , $HeightAfter*($picFinalWidth/$widthAfter));
        }
        else if($HeightAfter<$picFinalHeight)
        {
            $image-> resize($widthAfter*($picFinalHeight/$HeightAfter), $picFinalHeight);
        }

        // $widthAfterCheck = $image -> width();
        // $HeightAfterCheck = $image -> height();

        // $cropX=intval($widthAfterCheck/2-$picFinalWidth/2);
        // $cropY=intval($HeightAfterCheck/2-$picFinalHeight/2);

        // if($cropX==0)
        // {
        //     $cropX=intval($picFinalWidth/2-$widthAfterCheck/2);
        // }
        // if($cropY==0){
        //     $cropY=intval($picFinalHeight/2-$HeightAfterCheck/2);
        // }

        // // crop image
        // $image->crop($picFinalWidth, $picFinalHeight, $cropX, $cropY);

        return $image;
    }


    public static function resizeKvImage($img,$picFinalWidth,$picFinalHeight)
    {

        $image =  $img ;
        $originalWidth =  $image -> width();
        $originalHeight =  $image -> height();

        //kv 鎖寬
        $image-> resize($picFinalWidth , $originalHeight*($picFinalWidth/$originalWidth));


        $widthAfter = $image -> width();
        $HeightAfter = $image -> height();

        //check2
        if($widthAfter<$picFinalWidth)
        {
            $image-> resize($picFinalWidth , $HeightAfter*($picFinalWidth/$widthAfter));
        }
        else if($HeightAfter<$picFinalHeight)
        {
            $image-> resize($widthAfter*($picFinalHeight/$HeightAfter), $picFinalHeight);
        }

        $widthAfterCheck = $image -> width();
        $HeightAfterCheck = $image -> height();

        $cropX=intval($widthAfterCheck/2-$picFinalWidth/2);
        $cropY=intval($HeightAfterCheck/2-$picFinalHeight/2);

        if($cropX==0)
        {
            $cropX=intval($picFinalWidth/2-$widthAfterCheck/2);
        }
        if($cropY==0){
            $cropY=intval($picFinalHeight/2-$HeightAfterCheck/2);
        }

        // crop image
        $image->crop($picFinalWidth, $picFinalHeight, $cropX, $cropY);

        return $image;
    }

}