<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarOriginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_origins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('origin_id',20)->nullable();
            $table->string('name',50)->nullable();
            $table->string('last_name',50)->nullable();
            $table->string('vin', 20)->nullable();
            $table->string('license_plate_number', 20)->nullable();
            $table->string('car_model', 50)->nullable();
            $table->string('car_type', 20)->nullable();
            $table->string('car_listing_date', 10)->nullable();
            $table->string('member_listing_date',  10)->nullable();
            $table->integer('mileage')->nullable();
            $table->string('visit_date', 10)->nullable();
            $table->string('dealer',20)->nullable();
            $table->integer('valid')->default(0)->nullable();
            $table->timestamps();
            $table->integer('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_origins');
    }
}
