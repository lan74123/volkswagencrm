<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('place', 100)->nullable();
            $table->string('type', 10);
            $table->string('image', 150)->nullable();
            $table->datetime('start_date');
            $table->datetime('end_date');
            $table->text('description');
            $table->integer('valid')->default(1);
            $table->timestamps();
            $table->integer('oid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
