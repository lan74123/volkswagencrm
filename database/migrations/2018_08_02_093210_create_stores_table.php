<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dealer_id');
            $table->string('type', 1);
            $table->string('area', 10);
            $table->string('name', 50);
            $table->string('code', 10)->nullable();
            $table->string('tel', 20)->nullable();
            $table->string('fax', 20)->nullable();
            $table->string('address', 100)->nullable();
            $table->string('email', 200)->nullable();
            $table->string('emailcc', 200)->nullable();
            $table->string('servicetime', 100)->nullable();         
            $table->integer('valid')->default(0);
            $table->timestamps();
            $table->integer('oid')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
