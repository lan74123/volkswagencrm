<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 10);
            $table->string('title', 50);
            $table->string('short_desc', 200);
            $table->string('content_desc', 1000);
            $table->string('info', 500);
            $table->string('img_path', 200);
            $table->string('publish_date', 10);
            $table->integer('valid')->default(0);
            $table->string('url', 200)->nullable();       
            $table->integer('is_top')->default(0);
            $table->timestamps();
            $table->integer('oid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
