<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaintainOriginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maintain_origins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('origin_id',20);
            $table->string('vin', 20);
            $table->string('license_plate_number', 20);
            $table->string('dealer',20)->nullable();
            $table->string('charge',20)->nullable();
            $table->text('order_details')->nullable();
            $table->integer('mileage')->default(0);
            $table->string('close_date', 10)->nullable();
            $table->integer('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maintain_origins');
    }
}
