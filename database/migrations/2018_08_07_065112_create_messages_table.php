<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('message_groups_id');
            $table->integer('member_id');
            $table->string('type', 10);
            $table->string('send_time', 20);
            $table->string('title', 50);
            $table->string('content', 200);
            $table->integer('status')->default(0);
            $table->integer('send_status')->default(0);
            $table->string('img_path', 200)->nullable();
            $table->string('url', 200)->nullable();
            $table->integer('news_id')->default(0);
            $table->integer('valid')->default(1);
            $table->timestamps();
            $table->integer('oid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
