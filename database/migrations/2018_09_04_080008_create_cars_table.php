<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id');
            $table->integer('car_type_id');
            $table->string('origin_id',20)->nullable();
            $table->string('vin', 20);
            $table->string('license_plate_number', 20);
            $table->string('car_model', 50)->nullable();
            $table->string('car_type', 20)->nullable();
            $table->string('car_listing_date', 10)->nullable();
            $table->string('member_listing_date',  10)->nullable();
            $table->integer('mileage')->default(0);
            $table->string('visit_date', 10)->nullable();
            $table->integer('dealer_id')->default(0);
            $table->string('dealer', 30)->nullable();
            $table->integer('valid')->default(1);
            $table->timestamps();
            $table->integer('oid')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
